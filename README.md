What is Make a Diff?
====================
In a nutshell
-------------
Make a Diff is a crowdsourcing website ([https://makeadiff.org](https://makeadiff.org)) linking charities with technology experts. It gives the development industry the opportunity to use their powers for good, helping not-for-profits with their technology needs, for free.

Why is it needed?
-----------------
Time and time again we see charities struggling with technology. Generally they don't have the people, skills or money to really harness technology. There are IT professionals out there who specialise in helping not-for-profits. While they may do a good job, they cost money. A lot of charities don't have money to spend on these kinds of services, even if it will save them money in the long run.

How does it work?
-----------------
Make a Diff runs on volunteer contribution to open source projects. A charity comes along and registers a specific need, such as a new website, a mobile app, a logo or help deciding what software to buy; anything that needs some technology skills. Along come volunteers, with valuable skills and kind hearts, who work on a project for a charity. The work gets done, and the charities carry on making the world a better place.

The Pitch
---------
[![Youtube video](https://img.youtube.com/vi/FI4qizCsKC8/0.jpg)](https://www.youtube.com/watch?v=FI4qizCsKC8)

Contributing to Make a Diff
===========================
Anyone can contribute to the Make a Diff project! It is written as a JIRA plugin, customising a JIRA instance in certain ways (such as the home page and project page), whilst harnessing JIRA's powerful project and issue tracking.

Getting it up and running
-------------------------
Firstly, you'll need to get the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK). Then, from the `makeadiff-jira-plugin` directory, run

``atlas-run`` 

This will start a JIRA instance with the Make a Diff plugin installed. This is accessible at http://localhost:2990/jira/

In a separate terminal (also from the `makeadiff-jira-plugin` directory), run 

``atlas-cli``

This will allow you to use the `pi` command whenever you want to update the plugin with new code. This will build the plugin and install it on the JIRA instance. More info about this command is [here](https://developer.atlassian.com/display/DOCS/atlas-cli).

Make a Diff!
------------
The [backlog lives here](https://makeadiff.org/browse/MD/tasks). Pick something that interests you and go for it! When you're done, go ahead and commit. If you have write access to this repository, there's no need for a pull request. If you want some feedback though, feel free to open a PR and we'll take a look.

See your changes
----------------
Once your code is in, go check [the build](https://extranet-bamboo.internal.atlassian.com/browse/FOUNDATION-MAKEADIFF). If it's green, you'll see your changes immediately at [https://makeadiff.stg.internal.atlassian.com/](https://makeadiff.stg.internal.atlassian.com/). When we are happy with the changes, they go live at [https://makeadiff.org](https://makeadiff.org).


Success
-------
You're helping to make a difference! It must feel good. Revisit the backlog and do it again, you know you want to.

Lost? Confused? Want someone to talk to?
----------------------------------------
Drop us a line at [makeadiff@atlassian.com](mailto:makeadiff@atlassian.com) and we'll get you on the right track.
