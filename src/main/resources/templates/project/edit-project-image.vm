#* @vtlvariable name="action" type="com.atlassian.makeadiff.action.EditProjectImage" *#
#* @vtlvariable name="textutils" type="com.opensymphony.util.TextUtils" *#
#* @vtlvariable name="webResourceManager" type="com.atlassian.plugin.webresource.WebResourceManager" *#
#* @vtlvariable name="req" type="javax.servlet.http.HttpServletRequest" *#
<html>
<head>
    ${webResourceManager.requireResource("com.atlassian.makeadiff.makeadiff-jira-plugin:edit-project-image-web-resources")}
    <title>$action.getText('makeadiff.project.image.edit.title')</title>
</head>

<body>
<header class="aui-page-header">
    <div class="aui-page-header-inner">
        <div class="aui-page-header-main">
            <h1>${action.getText("makeadiff.project.image.edit.title")}</h1>
        </div>
    </div>
</header>

<div class="aui-page-panel">
    <div class="aui-page-panel-inner">
        <section class="aui-page-panel-content">
            <div class="preview-image-container" #if ($action.hasProjectImage == false)style="display: none"#end>
                <img class="preview-image"
                    #if ($action.hasProjectImage)
                        src="${req.contextPath}/secure/project-image?projectKey=${action.project.key}&size=big"
                    #end
                    >
                </img>
            </div>

            <form action="${req.contextPath}/secure/EditProjectImage.jspa" method="post" id="edit-project-image" class="aui">
                <div class="form-body">
                    <div class="hidden">
                        <input name="atl_token" type="hidden" value="${action.xsrfToken}"/>
                    </div>
                    <input type="hidden" name="key" value="${action.key}"/>
                    <input type="hidden" name="pid" value="${action.project.id}"/>
                    <input type="hidden" name="tempFileId" value=""/>

                    #if ($action.hasAnyErrors())
                        #parse("templates/errormessages.vm")
                    #end

                    <div class="field-group input-file-group">
                        <label for="tempFilename">
                            ${action.getText("makeadiff.project.image.form.tempfilename")}
                            <span class="aui-icon icon-required"></span>
                        </label>
                        <input class="ignore-inline-attach" name="tempFilename" id="tempFilename" type="file">
                        <div id="attach-max-size" class="hidden">10485760</div>
                        <div class="description">The maximum file upload size is 10.00 MB.</div>
                    </div>
                </div>
                <div class="buttons-container form-footer">
                    <div class="buttons">
                        #set ($submitAccessKey = ${i18n.getText("AUI.form.submit.button.accesskey")})
                        #set ($cancelAccessKey = ${i18n.getText("AUI.form.cancel.link.accesskey")})

                        <input accesskey="$submitAccessKey" class="aui-button"
                               disabled="disabled"
                               name="Create"
                               title="${action.getText("AUI.form.submit.button.tooltip", "${submitAccessKey}", "${action.modifierKey}")}"
                               type="submit"
                               value="${i18n.getText("makeadiff.project.edit.submit.label")}"/>
                        <a accesskey="${cancelAccessKey}"
                           class="aui-button aui-button-link cancel"
                           href="${req.contextPath}/browse/${action.key}"
                           title="${i18n.getText("AUI.form.cancel.link.tooltip", "${cancelAccessKey}", "${action.modifierKey}")}">
                            ${i18n.getText("AUI.form.cancel.link.text")}</a>
                    </div>
                </div>
            </form>
        </section>
        <!-- Replicate the layout of the browse project page, for the image preview size -->
        <!-- TODO find a better way to do this -->
        <aside class="aui-page-panel-sidebar" id="sidebar" style="visibility: hidden;">
            <p>Blah blah blah blah blah blah blah blah blah blah blah blah blah</p>
        </aside>
    </div>
</div>
</body>
</html>
