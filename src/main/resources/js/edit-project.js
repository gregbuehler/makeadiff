
(function ($) {

  $(document).ready(function() {
    var avatarPickerDialog,
      projectKey = $("input[name='key']").val(),
      projectId = $("#avatar-owner-id").text(),
      defaultAvatarId = $("#default-avatar-id").text();
    
      avatarPickerDialog = JIRA.createProjectAvatarPickerDialog({
        'trigger': '.jira-avatar-picker-trigger',
        'projectKey': projectKey,
        'projectId': projectId,
        'defaultAvatarId': defaultAvatarId,
        'select': function(e) {
          //assuming that the trigger is the avatar image itself.
          
          var src = $(".jira-avatar-picker-trigger").attr("src");
          $(".jira-avatar-picker-trigger").attr("src", src + "&amp;t=" + new Date().getTime());
          
        }
      });
  });

})(AJS.$);
