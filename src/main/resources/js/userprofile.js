AJS.toInit(function($) {
    var skills = [];
    var editAboutDialog,
        editOrgDialog;

    var EditMakeADiffProfileDialog = JIRA.EditProfileDialog.extend({
        _reload: function() {
            return true;
        }
    });

    new EditMakeADiffProfileDialog({
        trigger: "#edit_profile_lnk",
        autoClose: false
    });

    $("#edit_org_lnk").click(function(e) {
        e.preventDefault();
        if (!editOrgDialog) {
            editOrgDialog = new AJS.Dialog({
                id: "edit-organisations-dialog",
                height: 240
            });

            var $content = $(Makeadiff.Userprofile.editOrganisations({organisations: []}));

            // TODO make a shared component
            new AJS.SingleSelect({
                element: $content.find("select[name='organisation']"),
                //itemAttrDisplayed: "name",
                showDropdownButton: true,
                submitInputVal: true, // submit the input text even if the user has not clicked an item or pressed enter
                errorMessage: "",
                userEnteredOptionsMsg: "Add New",
                ajaxOptions: {
                    url: contextPath + "/rest/makeadiff/1/org/search",
                    query: true, // keep going back to the sever for each keystroke
                    formatResponse: function (orgs) {

                        var ret = [];
                        var groupDescriptor = new AJS.GroupDescriptor({
                            weight: 0,
                            label: "Already registered"
                        });
                        ret.push(groupDescriptor);

                        AJS.$(orgs).each(function() {
                            groupDescriptor.addItem(new AJS.ItemDescriptor({
                                value: this.name,
                                label: this.name,
                                html: this.name
                            }));
                        });

                        return ret;
                    }
                }
            });

            $content.submit(function(e) {
                e.preventDefault();
                var $orgField = $(this).find("select[name='organisation']"),
                    orgFieldVal = $orgField.val();
                if (orgFieldVal && orgFieldVal.length) {
                    var orgName = orgFieldVal[0];
                    $.ajax({
                        url: contextPath + "/rest/makeadiff/1/profile/org",
                        type: "PUT",
                        contentType: "application/json",
                        data: orgName,
                        success: function() {
                            $("#organizations-list").append($("<li/>").text(orgName));
                            $orgField.val();
                            editOrgDialog.hide();
                        }
                    });
                }
            });

            editOrgDialog.addHeader("Add Organisation");
            editOrgDialog.addPanel("SinglePanel", $content);
            editOrgDialog.addLink("Close", function(dialog) {
                dialog.hide();
                return false;
            });
        }

        $('#edit-organizations-list').html('');
        $("#organizations-list li").each(function(i, org) {
            var orgName = $(org).text().trim();
            $('#edit-organizations-list').append(
                $('<div/>').addClass('aui-label aui-label-closeable').text(orgName).data('name', orgName)
                    .append($('<span tabindex="0" class="aui-icon aui-icon-close" original-title="(remove closableNoUrl)">(remove closableNoUrl)</span>'))
            );
        });

        $('#edit-organizations-list .aui-icon-close').on('click', function() {
            var that = this;
            var orgName = $(this).parent().data('name');
            $.ajax({
                url: contextPath + "/rest/makeadiff/1/profile/org",
                type: "DELETE",
                contentType: "application/json",
                data: orgName
            }).done(function() {
                $(that).parent().remove();
                $("#organizations-list li").each(function(i, org) {
                    if ($(org).text().trim() === orgName) {
                        $(org).remove();
                    }
                });
            });
        });

        editOrgDialog.show();
    });

//    AJS.$.ajax({
//        type: "GET",
//        url: contextPath + "/rest/makeadiff/1/profile",
//        contentType: "application/json"
//    }).done(function(result){
//        console.log(result.skills);
//        if (result.skills != undefined) {
//            skills = result.skills.split(";");
//            console.log(skills);
//        }
//        console.log(result.description);
//        if (result.description != undefined) {
//            about = result.description;
//        }
//
//        // Add the skills list
//        var skillsList = AJS.$("#skills-list")
//        // Add the project list
//        AJS.$.each(skills, function(index, value){
//            skillsList.append(Makeadiff.Userprofile.renderSkillItem({skill: encodeURIComponent(value)}));
//        });
//
//
//        var currentPList = AJS.$("#current-project-list")
//        var completePList = AJS.$("#complete-project-list")
//        // Add the project list
//        AJS.$.each(project.current, function(index, value){
//            currentPList.append("<li>"+value+"</li>");
//        });
//        AJS.$.each(project.complete, function(index, value){
//            completePList.append("<li>"+value+"</li>");
//        });
//

    AJS.$("#edit_about_lnk").click(function(e){
        e.preventDefault();
        if (!editAboutDialog) {
            editAboutDialog = new AJS.Dialog({
                id: "edit-about-dialog",
                height: 300
            });

            var $content = $(Makeadiff.Userprofile.editAboutSectionForm({description:$("#about-text").text().trim()}));

            $content.submit(function(e) {
                e.preventDefault();
                var description = $("#about-textarea").val();

                if (description) {
                    $.ajax({
                        type: "PUT",
                        url: contextPath + "/rest/makeadiff/1/profile/description",
                        contentType: "application/json",
                        data: description
                    }).success(function(result){
                            var desc = result.description;
                            $('#about-text').text(desc);
                            $('#about-textarea').val(desc);
                            editAboutDialog.hide();
                    });
                }
            });

            editAboutDialog.addHeader("Edit About Section");
            editAboutDialog.addPanel("SinglePanel", $content);
            editAboutDialog.addButton("Update", function(dialog){
                $content.submit();
            });
            editAboutDialog.addCancel("Cancel", function(dialog) {
                $('#about-textarea').val($("#about-text").text().trim());
                dialog.hide();
            });
        }

        editAboutDialog.show();
    });



//
//        AJS.$("#edit_skills_lnk").click(function(){
//            $("#edit_skills_lnk").addClass("hidden");
//            console.log("clicked!");
//            AJS.$(".edit-skill-remove")
//                .removeClass("hidden")
//                .click(function(e){
//                    delSkill = AJS.$(this).parent().text().split(" ",1)[0];
//                    console.log(delSkill)
//                    AJS.$(this).parent().remove();
//                    var index = skills.indexOf(delSkill);
//                    console.log(index);
//                    skills.splice(index, 1);
//                    console.log(skills);
//                });
//            AJS.$("#add-skill-container").append(
//                '<input type="text" id="new-skill" value="" maxlength="100" />  <span class="aui-icon aui-icon-small aui-iconfont-add edit-skill-add" >Add</span> <br /> <button id="save-edit-skill" class="aui-button aui-button-primary">Save</button>'
//            );
//
//            AJS.$(".edit-skill-add").click(function(){
//                newSkill = AJS.$("#new-skill").val();
//                console.log(AJS.$.inArray(newSkill, skills));
//                if (AJS.$.inArray(newSkill, skills) == -1 && newSkill != "") {
//                    console.log("new skill!!");
//                    skills.push(newSkill);
//                    skillsList.append(Makeadiff.Userprofile.renderSkillItem({skill: newSkill}));
//                    AJS.$(".edit-skill-remove").last()
//                        .removeClass("hidden")
//                        .click(function(e){
//                            delSkill = AJS.$(this).parent().text().split(" ",1)[0];
//                            console.log(delSkill)
//                            AJS.$(this).parent().remove();
//                            var index = skills.indexOf(delSkill);
//                            console.log(index);
//                            skills.splice(index, 1);
//                            console.log(skills);
//                        });
//                }
//                AJS.$("#new-skill").val("");
//            });
//
//            AJS.$("#save-edit-skill").click(function(){
//                $("#edit_skills_lnk").removeClass("hidden");
//                AJS.$(".edit-skill-remove").addClass("hidden").unbind('click');
//                AJS.$("#add-skill-container").empty();
//
//                AJS.$.ajax({
//                    type: "PUT",
//                    url: contextPath + "/rest/makeadiff/1/profile/skills",
//                    contentType: "application/json",
//                    data: skills.join(";")
//                }).done(function(result){
//                        alert("done")
//                    }).fail(function(result){
//                        alert("fail");
//                    });
//            });
//        });
//    });
});