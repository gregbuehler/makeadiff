AJS.toInit(function($) {

    function updateCharityFieldVisibility() {
        if ($("#belongsToOrg").is(":checked")) {
            $("#org-details").show();
        } else {
            $("#org-details").hide();
        }
    }

    $("#belongsToOrg").change(function() {
        updateCharityFieldVisibility();
    });

    updateCharityFieldVisibility();

    new AJS.SingleSelect({
        element: AJS.$("#organisation"),
//        itemAttrDisplayed: "name",
        showDropdownButton: true,
        submitInputVal: true, // submit the input text even if the user has not clicked an item or pressed enter
        errorMessage: "",
        userEnteredOptionsMsg: "Add New",
        ajaxOptions: {
            url: contextPath + "/rest/makeadiff/1/org/search",
            query: true, // keep going back to the sever for each keystroke
            formatResponse: function (orgs) {

                var ret = [];
                var groupDescriptor = new AJS.GroupDescriptor({
                    weight: 0,
                    label: "Already registered"
                });
                ret.push(groupDescriptor);

                AJS.$(orgs).each(function() {
                    groupDescriptor.addItem(new AJS.ItemDescriptor({
                        value: this.name,
                        label: this.name,
                        html: this.name
                    }));
                });

                return ret;
            }
        }
    });
});