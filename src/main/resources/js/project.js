(function ($) {
    // Get browser to re-fetch all avatar icons on DOM ready in case of stale image
    $(document).ready(function() {
        $(".aui-avatar-inner").each(function(key, target) {
          var img = $(target).find("img");
          if (img.attr("src").indexOf("projectavatar") > 0) {
            var avatarSrc = img.attr("src");
            img.attr("src", avatarSrc + "&t=" + new Date().getTime());
          }
        });
    });

    AJS.toInit(function() {
        // Add the Make a Diff project header
        var $header = $(MakeADiff.Templates.Project.header({
            projectId: WRM.data('makeadiff-project.id'),
            projectKey: WRM.data('makeadiff-project.key'),
            projectName: WRM.data('makeadiff-project.name'),
            hasCreateIssuePermission: WRM.data('makeadiff-project.hasCreateIssuePermission'),
            hasProjectAdminPermission: WRM.data('makeadiff-project.hasProjectAdminPermission'),
            currentPage: WRM.data('makeadiff-project.page')
        }));
        $("#content").prepend($header);

        // bind volunteer champion button
        $("#volunteer-champion").click(function(){
            var url = AJS.contextPath() + "/rest/makeadiff/1/projects/" + AJS.Meta.get("projectKey") + '/champion';
            $.ajax({
                url: url,
                type: "POST",
                success: function (html) {
                    location.reload();
                }
            });
            //todo error/spinner handling
        });
    });

})(AJS.$);

