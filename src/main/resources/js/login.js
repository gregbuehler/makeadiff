AJS.toInit(function($) {
    var $form = $("#foundation-log-in");

    function clearErrors() {
        $form.find(".error").remove();
    }

    function showError(msg) {
        $form.find(".form-body").prepend($("<div/>")
            .addClass("aui-message error")
            .text(msg));
    }

    function login(username, password) {

        var loginDetails = {
            "username": username,
            "password": password
            };

        ajaxOptions= {
            url: contextPath + "/rest/auth/1/session",
            data: JSON.stringify(loginDetails),
            type: 'POST',
            contentType: 'application/json'
        };

        $form.find("input.text").val('');

        $.ajax(ajaxOptions)
            .done(function( data ){
                window.location.href = contextPath + "/view/home";
            })
            .fail(function(jqXHR, textStatus, errorThrown ){
                if (errorThrown === 'Unauthorized'){
                    showError(AJS.I18n.getText("makeadiff.login.invalid"));
                } else {
                    showError(AJS.I18n.getText("makeadiff.login.error"));
                }
            });
    }

    $form.submit(function(){
        clearErrors();
        login($form.find("input#email").val(), $form.find("input#password").val());
        return false;
    });


});