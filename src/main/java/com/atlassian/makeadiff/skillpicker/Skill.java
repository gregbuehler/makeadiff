package com.atlassian.makeadiff.skillpicker;

/**
 * List of skills the Make a Diff platform considers relevant for charity projects.
 */
public enum Skill
{
    DESIGN_UI("User Interface Design"),
    DESIGN_UX("User Experience Design (UX)"),
    DESIGN_VISUAL("Visual Design"),
    DESIGN_IA("Information Architecture (IA)"),
    DESIGN_PHOTOSHOP("Adobe PhotoShop"),
    DESIGN_ILLUSTRATOR("Adobe Illustrator"),
    DESIGN_FIREWORKS("Adobe Fireworks"),

    PM("Project Management"),
    BA("Business Analysis"),

    MARKETING_SEO("Search Engine Optimization (SEO)"),
    MARKETING_SEM("Search Engine Marketing (SEM)"),
    MARKETING_SMS("Social Media Strategy"),
    MARKETING_EDM("Email Direct Marketing (EDM)"),

    DEV_DOT_NET(".NET"),
    DEV_JS("Javascript"),
    DEV_JAVA("Java"),
    DEV_PHP("PHP"),
    DEV_RUBY("Ruby"),
    DEV_PYTHON("Python"),
    DEV_PERL("Perl"),
    DEV_SQL("SQL"),
    DEV_CSS("CSS"),

    FRAMEWORK_DRUPAL("Drupal"),
    FRAMEWORK_JOOMLA("Joomla"),
    FRAMEWORK_MAGNOLIA("Magnolia"),
    FRAMEWORK_SALESFORCE("SalesForce"),
    FRAMEWORK_SHAREPOINT("Sharepoint"),
    FRAMEWORK_WORDPRESS("Wordpress");

    private String label;

    private Skill(String label)
    {
        this.label = label;
    }

    public static Skill from(String label)
    {
        for (Skill skill : Skill.values())
        {
            if (skill.getLabel().equals(label))
            {
                return skill;
            }
        }

        return null;
    }

    public String getLabel()
    {
        return label;
    }

    @Override
    public String toString()
    {
        return label;
    }
}
