package com.atlassian.makeadiff.skillpicker;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import java.util.*;

public class SkillCFType extends AbstractMultiCFType<Skill>
{
    private final JiraAuthenticationContext authContext;
    private final IssueManager issueManager;

    public SkillCFType(JiraAuthenticationContext authContext, IssueManager issueManager, CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager)
    {
        super(customFieldValuePersister, genericConfigManager);

        this.authContext = authContext;
        this.issueManager = issueManager;
    }

    @Nullable
    @Override
    protected Comparator<Skill> getTypeComparator()
    {
        return new Comparator<Skill>()
        {
            @Override
            public int compare(Skill o1, Skill o2)
            {
                return o1.compareTo(o2);
            }
        };
    }

    @Nullable
    @Override
    protected Object convertTypeToDbValue(@Nullable Skill value)
    {
        return getStringFromSingularObject(value);
    }

    @Nullable
    @Override
    protected Skill convertDbValueToType(@Nullable Object dbValue)
    {
        return getSingularObjectFromString(dbValue.toString());
    }

    @Override
    protected PersistenceFieldType getDatabaseType()
    {
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    @Override
    public String getStringFromSingularObject(Skill singularObject)
    {
        if (singularObject == null)
        {
            return null;
        }

        return singularObject.getLabel();
    }

    @Override
    public Skill getSingularObjectFromString(String string) throws FieldValidationException
    {
        return Skill.from(string);
    }

    @Override
    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config)
    {

    }

    @Override
    public Collection<Skill> getValueFromCustomFieldParams(CustomFieldParams parameters) throws FieldValidationException
    {
        final Collection<?> values = parameters.getAllValues();
        if (CustomFieldUtils.isCollectionNotEmpty(values))
        {
            List<Skill> options = new ArrayList<Skill>();
            for (Object value : values)
            {
                options.add(getSingularObjectFromString((String) value));
            }
            return options;
        }
        else
        {
            return null;
        }
    }

    @Override
    public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters)
    {
        return parameters.getValuesForNullKey();
    }

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
        final Map<String, Object> velocityParameters = super.getVelocityParameters(issue, field, fieldLayoutItem);

        final Issue issueFromDb = issueManager.getIssueObject(issue.getId());
        velocityParameters.put("canEdit", issueManager.isEditable(issueFromDb, authContext.getLoggedInUser()));
        velocityParameters.put("skills", Sets.newHashSet(Skill.values()));

        velocityParameters.put("fieldId", field.getId());
        velocityParameters.put("i18n", authContext.getI18nHelper());
        velocityParameters.put("field", field);
        velocityParameters.put("issue", issue);

        return velocityParameters;
    }
}
