package com.atlassian.makeadiff.rest;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 23/03/13
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */


import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.makeadiff.api.TaskCreationProperties;
import com.atlassian.makeadiff.api.TaskService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * REST resource for dealing with "Make a Diff" projects
 */
@Path("tasks")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TaskServiceResource extends BaseResource
{
    private final Long pIdtest;
    private TaskService taskService;
    private final IssueTypeManager issueTypeManager;

    private TaskServiceResource(final TaskService taskService,
                                final IssueTypeManager issueTypeManager,
                                ProjectManager pm
    )
    {
        this.taskService = taskService;
        this.issueTypeManager = issueTypeManager;

        // TODO remove later, need for testing
        //pIdtest = pm.getProjectObjByKey("TEST").getId();
        pIdtest = null;
    }

    @GET
    @Path("{id}")
    public Issue get(@PathParam("id") Long id)
    {
        return taskService.getTask(id);
    }

    @POST
    public Response create(TaskModel taskModel)
    {
        TaskCreationProperties taskCreationProperties = new TaskCreationProperties();
        taskCreationProperties.setSummary(taskModel.name);
        taskCreationProperties.setDescription(taskModel.description);
        // TODO use correct issue type
        taskCreationProperties.setIssueTypeId(issueTypeManager.getIssueTypes().iterator().next().getId());
        taskCreationProperties.setProjectId(pIdtest);
        //taskCreationProperties.setProjectId(taskModel.projectId);
        taskCreationProperties.setSummary(taskModel.summary);
        TaskService.CreateValidationResult validationResult = taskService.validateCreateTask(taskCreationProperties);
        if (!validationResult.isValid())
        {
            return error(validationResult.getErrorCollection());
        }

        // taskService.createTask(validationResult);

        return Response.ok(createResult(validationResult.getIssue())).build();

    }

    private TaskModel createResult(final Issue issue)
    {
        final TaskModel model = new TaskModel()
        {{
                id = String.valueOf(issue.getId());
                name = "fg";
                description = issue.getDescription();
                projectId = issue.getProjectObject().getId();
                summary = issue.getSummary();
            }};

        return model;
    }

    @GET
    @Path("search/{projectId}")
    public Response searchTask(@PathParam("projectId") Long projectId)
    {
        List<Issue> issues = taskService.getIssues(projectId);
        Collection<TaskModel> toReturn = new ArrayList<TaskModel>();
        for (Issue taskModel : issues)
        {
            toReturn.add(createResult(taskModel));
        }
        return Response.ok(toReturn).build();
    }

    public static class TaskModel
    {
        @JsonProperty
        public String id;

        @JsonProperty
        public String name;

        @JsonProperty
        public String description;

        @JsonProperty
        public Long projectId;

        @JsonProperty
        public String summary;
    }

}
