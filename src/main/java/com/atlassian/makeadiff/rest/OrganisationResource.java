package com.atlassian.makeadiff.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("org")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class OrganisationResource
{
    private final JiraAuthenticationContext jiraContext;
    private final OrganisationService organisationService;

    public OrganisationResource(final OrganisationService organisationService, final JiraAuthenticationContext context)
    {
        this.organisationService = organisationService;
        jiraContext = context;
    }

    @POST
    @AnonymousAllowed
    public Response createOrg(final CreateOrganisationBean bean)
    {
        if (bean == null || StringUtils.isEmpty(bean.name))
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final Organisation org = organisationService.createOrUpdate(bean.name, bean.url);
        return Response.status(Response.Status.CREATED).entity(toBean(org)).build();
    }

    @PUT
    @Path("addMe")
    public Response addMember(@QueryParam("name") final String name)
    {
        final ApplicationUser user = jiraContext.getUser();
        final Organisation org = organisationService.getOrganisationByName(name);
        if (org == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        organisationService.addUserToOrganisation(user.getKey(), org);
        return Response.ok().build();
    }

    @PUT
    @Path("verify")
    public Response verify(@QueryParam("id") final int id)
    {
        Organisation currentOrganisation = organisationService.getOrganisationById(id);

        organisationService.edit(currentOrganisation.getName(), currentOrganisation.getUrl(), currentOrganisation.getDescription(), true);

        return Response.ok().build();
    }

    @GET
    @AnonymousAllowed
    @Path("search")
    public Response search(@QueryParam("query") final String query)
    {
        return Response.ok(toBeans(organisationService.search(query))).build();
    }

    private static Collection<OrganisationBean> toBeans(final Collection<Organisation> organisations)
    {
        return Collections2.transform(organisations, new Function<Organisation, OrganisationBean>()
        {
            @Override
            public OrganisationBean apply(@Nullable final Organisation organisation)
            {
                return toBean(organisation);
            }
        });
    }

    private static OrganisationBean toBean(final Organisation org)
    {
        return new OrganisationBean(org.getId(), org.getKey(), org.getName(), org.getUrl(), org.getDescription(), org.getVerified());
    }

    public static class CreateOrganisationBean
    {
        @JsonProperty
        public String name;

        @JsonProperty
        public String url;
    }

    public static class OrganisationBean
    {
        @JsonProperty
        private String key;

        @JsonProperty
        private String name;

        @JsonProperty
        private String url;

        @JsonProperty
        private int id;

        @JsonProperty
        private String description;

        @JsonProperty
        private boolean verified;

        public OrganisationBean(final int id, final String key, final String name, final String url, final String description, final boolean verified)
        {
            this.id = id;
            this.key = key;
            this.name = name;
            this.url = url;
            this.description = description;
            this.verified = verified;
        }
    }
}
