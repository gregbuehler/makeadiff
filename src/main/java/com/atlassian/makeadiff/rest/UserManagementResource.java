package com.atlassian.makeadiff.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.UserProfile;
import com.atlassian.makeadiff.api.UserProfileService;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Path("user")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class UserManagementResource extends BaseResource
{
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final UserUtil userUtil;
    private final UserProfileService userProfileService;

    public UserManagementResource(final JiraAuthenticationContext authenticationContext, 
                                  final PermissionManager permissionManager, 
                                  final UserUtil userUtil, 
                                  final UserProfileService userProfileService) 
    {
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.userUtil = userUtil;
        this.userProfileService = userProfileService;
    }

    @GET
    public Response get()
    {
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getUser()))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        return Response.ok(getAllUsers()).build();
    }

    @GET
    @Path("tsv")
    @Produces({MediaType.TEXT_PLAIN})
    public Response getTsv()
    {
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getUser()))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        return Response.ok(asTsv(getAllUsers())).build();
    }

    private List<UserBean> getAllUsers()
    {
        return new ArrayList<UserBean>(Collections2.transform(userUtil.getAllApplicationUsers(), new Function<ApplicationUser, UserBean>()
        {
            @Override
            public UserBean apply(final ApplicationUser appUser)
            {
                return toBean(appUser, userProfileService.getUserProfile(appUser.getKey()));
            }
        }));
    }

    private static UserBean toBean(final ApplicationUser user, final UserProfile userProfile)
    {
        UserBean bean = new UserBean();
        bean.username = user.getUsername();
        bean.displayName = user.getDisplayName();
        bean.email = user.getEmailAddress();
        bean.organisations = getOrganisationNames(userProfile.getOrganisations());
        return bean;
    }

    private static Collection<String> getOrganisationNames(final Collection<Organisation> organisations)
    {
        if (organisations == null || organisations.isEmpty())
        {
            return Collections.emptyList();
        }

        return Collections2.transform(organisations, new Function<Organisation, String>()
        {
            @Override
            public String apply(final Organisation org)
            {
                return org.getName();
            }
        });
    }

    /**
     * Return list as tab-separated-value string
     */
    private static String asTsv(final List<UserBean> users)
    {
        final StringBuilder sb = new StringBuilder();

        sb.append("username\tdisplayName\temail\torganisations\n");

        for (final UserBean user : users)
        {
            sb.append(user.username).append("\t")
                    .append(user.displayName).append("\t")
                    .append(user.email).append("\t")
                    .append(Joiner.on(",").join(user.organisations)).append("\n");
        }

        return sb.toString();
    }

    public static class UserBean
    {
        @JsonProperty
        public String username;

        @JsonProperty
        public String displayName;

        @JsonProperty
        public String email;

        @JsonProperty
        public Collection<String> organisations;
    }
}