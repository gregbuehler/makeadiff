package com.atlassian.makeadiff.rest;

import com.atlassian.jira.util.ErrorCollection;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

public abstract class BaseResource
{

    protected Response error(ErrorCollection errorCollection)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(toModel(errorCollection)).build();
    }


    private ErrorModel toModel(ErrorCollection errorCollection)
    {
        ErrorCollection.Reason reason = errorCollection.getReasons().iterator().next();
        ErrorModel model = new ErrorModel();
        model.message = reason.name();
        model.errors = errorCollection.getErrors();
        return model;
    }

    @XmlRootElement
    private class ErrorModel
    {

        @XmlElement
        String message;
        @XmlElement
        Map<String, String> errors;

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public Map<String, String> getErrors()
        {
            return errors;
        }

        public void setErrors(Map<String, String> errors)
        {
            this.errors = errors;
        }
    }
}
