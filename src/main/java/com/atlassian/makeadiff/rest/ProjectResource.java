package com.atlassian.makeadiff.rest;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.makeadiff.api.ImageDescriptor;
import com.atlassian.makeadiff.api.ProjectImageService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * REST resource for dealing with "Make a Diff" projects
 */
@Path("project")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ProjectResource extends BaseResource
{
    private final FoundationProjectService foundationProjectService;
    private final JiraAuthenticationContext authContext;
    private final ProjectImageService projectImageService;
    private final AttachmentHelper attachmentHelper;

    public ProjectResource(final FoundationProjectService foundationProjectService, final JiraAuthenticationContext authContext,
                           final ProjectImageService projectImageService, final AttachmentHelper attachmentHelper)
    {
        this.foundationProjectService = foundationProjectService;
        this.authContext = authContext;
        this.projectImageService = projectImageService;
        this.attachmentHelper = attachmentHelper;
    }

    @POST
    @Consumes(MediaType.WILDCARD)
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{projectKey}/image/upload")
    public Response uploadImage(final @PathParam("projectKey") String projectKey, final @QueryParam("filename") String filename, final @Context HttpServletRequest request)
    {
        if (StringUtils.isBlank(projectKey))
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        final FoundationProject project = foundationProjectService.getProject(projectKey);
        if (project == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (!foundationProjectService.isAllowedToEdit(authContext.getUser(), project))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        final AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, filename, null);
        if (!validationResult.isValid())
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(validationResult.getErrorMessage()).build();
        }

        final ImageDescriptor imageDescriptor = new ImageDescriptor(validationResult.getInputStream(), validationResult.getContentType(), filename);
        final ServiceOutcome<String> result = projectImageService.uploadTempImage(imageDescriptor);
        if (!result.isValid())
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final ImageUploadResult entity = new ImageUploadResult(result.getReturnedValue());
        return Response.status(Response.Status.CREATED).entity(entity).build();
    }

    @PUT
    @Path("{projectKey}/image/save")
    public Response saveImage(final @PathParam("projectKey") String projectKey, final @QueryParam("tempFileId") String tempFileId)
    {
        if (StringUtils.isBlank(projectKey))
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        final FoundationProject project = foundationProjectService.getProject(projectKey);
        if (project == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (!foundationProjectService.isAllowedToEdit(authContext.getUser(), project))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        try
        {
            final boolean success = projectImageService.saveProjectImage(projectKey, tempFileId);
            if (!success)
            {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return Response.noContent().build();
    }

    private static class ImageUploadResult
    {
        @JsonProperty
        private String tempFileId;

        private ImageUploadResult(final String tempFileId)
        {
            this.tempFileId = tempFileId;
        }
    }
}
