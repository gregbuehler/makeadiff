package com.atlassian.makeadiff.rest;

import org.codehaus.jackson.annotate.JsonProperty;

public class ProjectModel
{
    @JsonProperty
    public String name;

    @JsonProperty
    public String description;

    @JsonProperty
    public String url;

    @JsonProperty
    public String location;
    public String repo;
}
