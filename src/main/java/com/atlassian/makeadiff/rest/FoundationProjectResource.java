package com.atlassian.makeadiff.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.makeadiff.api.FoundationProjectStore;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST resource for dealing with "Make a Diff" projects
 */
@Path("projects")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class FoundationProjectResource extends BaseResource
{
    private final FoundationProjectService foundationProjectService;
    private final JiraAuthenticationContext authContext;
    private final FoundationProjectStore foundationProjectStore;
    private final PermissionManager permissionManager;

    public FoundationProjectResource(final FoundationProjectService foundationProjectService,
                                     final JiraAuthenticationContext authContext,
                                     final FoundationProjectStore foundationProjectStore,
                                     final PermissionManager permissionManager)
    {
        this.foundationProjectService = foundationProjectService;
        this.authContext = authContext;
        this.foundationProjectStore = foundationProjectStore;
        this.permissionManager = permissionManager;
    }

    @GET
    @Path ("/{projectKey}")
    public Response get(@PathParam ("projectKey") final String projectKey)
    {
        final FoundationProject project = foundationProjectService.getProject(projectKey);
        if (project == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        final ApplicationUser user = authContext.getUser();
        final boolean hasCreateIssuePermission = permissionManager.hasPermission(Permissions.CREATE_ISSUE, project, user);
        final boolean hasProjectAdminPermission = permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, user);
        final ProjectBean bean = new ProjectBean(project.getId(), project.getKey(), project.getName(), hasCreateIssuePermission, hasProjectAdminPermission);
        return Response.ok(bean).build();
    }

    @POST
    public Response create(ProjectModel projectModel)
    {
        final FoundationProjectEntity entity = new FoundationProjectEntity();
        final String username = (authContext.getUser() == null) ? null : authContext.getUser().getUsername();
        entity.setCreatorUserKey(username);

        FoundationProjectService.ValidationResult validationResult = foundationProjectService.validateCreateProject(projectModel.name, projectModel.description, entity, projectModel.url, projectModel.location, projectModel.repo);
        if (!validationResult.isValid())
        {
            System.out.println("Returning an error collection.");
            return error(validationResult.getErrorCollection());
        }

        foundationProjectService.createProject(validationResult);

        return Response.ok(entity).build();
    }

    @POST
    @Path("/{key}/champion")
    public Response championVolunteer(@PathParam("key") String projectKey)
    {
        if (!authContext.isLoggedInUser())
        {
            return Response.status(401).build();
        }

        boolean success = foundationProjectService.addChampion(projectKey, authContext.getUser());
        return success ? Response.ok().build() : Response.serverError().build();
    }

    @GET
    @Path("/{key}/agileboard")
    public Response getAgileBoard(@PathParam("key") String projectKey)
    {
        final FoundationProjectEntity project = foundationProjectStore.getProject(projectKey);
        return Response.ok(project.getAgileBoardId()).build();
    }

    @POST
    @Path("/{key}/agileboard")
    public Response fixAgileBoard(@PathParam("key") String projectKey, @QueryParam("agileBoardId") Long agileBoardId)
    {
        final FoundationProjectEntity project = foundationProjectStore.getProject(projectKey);
        project.setAgileBoardId(agileBoardId);
        foundationProjectStore.updateProject(project);
        return Response.ok().build();
    }
}
