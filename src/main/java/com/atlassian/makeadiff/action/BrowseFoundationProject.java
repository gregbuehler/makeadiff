package com.atlassian.makeadiff.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.exception.VelocityException;

import java.util.*;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * Webwork action for the project page.
 */
public class BrowseFoundationProject extends JiraWebActionSupport
{
    private static final String DEFAULT_TAB = "overview";

    private final FoundationProjectService foundationProjectService;
    private final VelocityTemplatingEngine velocityTemplatingEngine;
    private final JiraAuthenticationContext authenticationContext;
    private final JiraWebResourceManager webResourceManager;
    private final TaskService taskService;
    private final IssueManager issueManager;
    private final ProjectImageService projectImageService;
    private final PermissionManager permissionManager;

    private FoundationProject project;
    private boolean contentOnly;
    private String selectedTab;
    private String key;

    public BrowseFoundationProject(final FoundationProjectService foundationProjectService,
                                   final VelocityTemplatingEngine velocityTemplatingEngine,
                                   final JiraAuthenticationContext authenticationContext,
                                   final JiraWebResourceManager webResourceManager,
                                   final TaskService taskService,
                                   final IssueManager issueManager,
                                   final ProjectImageService projectImageService,
                                   final PermissionManager permissionManager)
    {
        this.foundationProjectService = foundationProjectService;
        this.velocityTemplatingEngine = velocityTemplatingEngine;
        this.authenticationContext = authenticationContext;
        this.webResourceManager = webResourceManager;
        this.taskService = taskService;
        this.issueManager = issueManager;
        this.projectImageService = projectImageService;
        this.permissionManager = permissionManager;
    }

    @Override
    protected String doExecute() throws Exception
    {
        webResourceManager.putMetadata("projectKey", key);
        return super.doExecute();
    }

    public FoundationProject getProject()
    {
        if (project == null)
        {
            project = foundationProjectService.getProject(key);
        }
        return project;
    }

    public boolean getShowChampionNeeded()
    {
        return getLoggedInApplicationUser() != null && !isCurrentUserCreator() && !isCurrentUserChampion() && getChampions().isEmpty();
    }

    public boolean isCurrentUserCreator()
    {
        return foundationProjectService.isUserCreator(getLoggedInApplicationUser(), getProject());
    }

    public boolean isCurrentUserChampion()
    {
        return foundationProjectService.isUserChampion(getLoggedInApplicationUser(), getProject());
    }

    public boolean isExists()
    {
        return (getProject() != null);
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public String getName()
    {
        return getProject().getName();
    }

    public boolean isContentOnly()
    {
        return contentOnly;
    }

    public void setContentOnly(final boolean contentOnly)
    {
        this.contentOnly = contentOnly;
    }

    public String getSelectedTab()
    {
        return (StringUtils.isBlank(selectedTab)) ? DEFAULT_TAB : selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public String getTabContentWithHtml()
    {
        final Map<String, Object> velocityParams = createVelocityParams();

        try
        {
            return velocityTemplatingEngine.render(file("templates/project/project-tab-" + getSelectedTab() + ".vm")).applying(velocityParams).asHtml();
        }
        catch (VelocityException e)
        {
            log.error(e);
            return "";
        }
    }

    public List<Issue> getAllTasks()
    {
        final ArrayList<Long> issueIds = new ArrayList<Long>();
        for (Issue issue : taskService.getIssues(project.getKey()))
        {
            issueIds.add(issue.getId());
        }

        return (issueIds.isEmpty()) ? Collections.<Issue>emptyList() : issueManager.getIssueObjects(issueIds);
    }

    public List<Issue> getUnresolvedTasks()
    {
        final ArrayList<Long> issueIds = new ArrayList<Long>();
        for (Issue issue : taskService.getIssues(project.getKey()))
        {
            if (issue.getResolutionObject() == null)
            {
                issueIds.add(issue.getId());
            }
        }

        return (issueIds.isEmpty()) ? Collections.<Issue>emptyList() : issueManager.getIssueObjects(issueIds);
    }

    public int getToDoTasksCount()
    {
        return taskService.getToDoTasksCount(getProject().getKey());
    }

    public int getInProgressTasksCount()
    {
        return taskService.getInProgressTasksCount(getProject().getKey());
    }

    public int getDoneTasksCount()
    {
        return taskService.getDoneTasksCount(getProject().getKey());
    }

    public List<FoundationProjectContributor> getRemainingContributors(int start)
    {
        List<FoundationProjectContributor> contributors = this.getContributors();

        if (start < contributors.size())
        {
            return contributors.subList(start, contributors.size());
        }
        else
        {
            return null;
        }
    }

    public List<FoundationProjectContributor> getSomeContributors(int start, int end)
    {
        List<FoundationProjectContributor> contributors = this.getContributors();
        if (end < contributors.size())
        {
            return contributors.subList(start, end);
        }
        else
        {
            return contributors.subList(start, contributors.size());
        }

    }

    public List<FoundationProjectContributor> getAllContributors()
    {
        return project.getAllContributors();
    }

    public List<FoundationProjectContributor> getContributors()
    {
        List<FoundationProjectContributor> result = new ArrayList<FoundationProjectContributor>();
        for (FoundationProjectContributor contributor : project.getAllContributors())
        {
            if(contributor.getContributorType() == FoundationProjectContributor.ContributorType.CONTRIBUTOR)
            {
                result.add(contributor);
            }
        }
        return result;
    }

    public List<FoundationProjectContributor> getChampions()
    {
        List<FoundationProjectContributor> result = new ArrayList<FoundationProjectContributor>();
        for (FoundationProjectContributor contributor : project.getAllContributors())
        {
             if(contributor.getContributorType() == FoundationProjectContributor.ContributorType.CHAMPION)
             {
                 result.add(contributor);
             }
        }
        return result;
    }

    public FoundationProjectContributor getCreator()
    {
        for (FoundationProjectContributor contributor : project.getAllContributors())
        {
            if(contributor.getContributorType() == FoundationProjectContributor.ContributorType.OWNER)
            {
                return contributor;
            }
        }
        return null;
    }
    
    public List<FoundationProjectContributor> getRankedContributors()
    {
    
    	// Took me too long to work out this existed...feels good, man. - Kerrod :)
    	List<FoundationProjectContributor> rankList = project.getAllContributors();
    	// Lets order this list
    	Collections.sort(rankList, new Comparator<FoundationProjectContributor>() {
    		  public int compare(FoundationProjectContributor con1, FoundationProjectContributor con2) {
    		    if (con1.getContributions() > con2.getContributions()) return -1;
    		    if (con1.getContributions() < con2.getContributions()) return 1;
    		    return 0;
    		  }});
    	
        return rankList;
    }

    public boolean getHasCreateIssuePermission()
    {
        return permissionManager.hasPermission(Permissions.CREATE_ISSUE, getProject(), getLoggedInApplicationUser());
    }

    public boolean getHasProjectAdminPermission()
    {
        return permissionManager.hasPermission(Permissions.PROJECT_ADMIN, getProject(), getLoggedInApplicationUser());
    }

    private Map<String, Object> createVelocityParams()
    {
        final Map<String, Object> velocityParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
        velocityParams.put("action", this);
        return velocityParams;

    }

    public boolean isAllowedToEdit()
    {
        return foundationProjectService.isAllowedToEdit(getLoggedInApplicationUser(), getProject());
    }

    public boolean isHasProjectImage()
    {
        return projectImageService.hasBannerImage(key);
    }
}
