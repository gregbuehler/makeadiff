package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.OrganisationStore;

/**
 * Webwork action for the edit organisation page.
 */
public class EditOrganisation extends JiraWebActionSupport
{
    private int organisationId;
    private Organisation organisation;

    private final OrganisationStore store;

    private final OrganisationService organisationService;

    private String url;
    private String description;
    private String name;

    public EditOrganisation(final OrganisationStore store, final OrganisationService organisationService)
    {
        this.store = store;
        this.organisationService = organisationService;
    }

    public java.lang.String doDefault() throws java.lang.Exception
    {
        setOrganisation(store.getOrganisationById(organisationId));
        return INPUT;
    }

    public String doExecute() throws Exception
    {
        setOrganisation(store.getOrganisationById(organisationId));

        if (url == null)
        {
            //TODO: resolve why this submits null values on page refresh and remove hacky work-around
            url = organisation.getUrl();
            description = organisation.getDescription();
        }

        organisationService.edit(organisation.getName(), url, description, organisation.getVerified());
        return SUCCESS;
    }

    public void setOrganisation(Organisation organisation)
    {
        this.organisation = organisation;
    }

    public Organisation getOrganisation()
    {
        return this.organisation;
    }

    public boolean noMembers()
    {
        /*
        if(organisation.getMembers() == null){
            return true;
        }
        else if(organisation.getMembers().size() == 0){
            return true;
        }
        */

        return false;
    }

    public void setOrganisationId(int organisationId)
    {
        this.organisationId = organisationId;
    }

    public void setId(int id)
    {
        organisationId = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getDescription()
    {

        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

}
