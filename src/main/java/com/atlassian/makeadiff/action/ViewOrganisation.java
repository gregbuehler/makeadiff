package com.atlassian.makeadiff.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.OrganisationStore;

import java.util.Collection;

/**
 * Webwork action for the create organisation page.
 */
public class ViewOrganisation extends JiraWebActionSupport
{
    private int organisationId;
    private Organisation organisation;
    private Collection<User> members;

    private final JiraAuthenticationContext jiraContext;
    private final OrganisationStore store;
    private final OrganisationService organisationService;

    public ViewOrganisation(final OrganisationStore store, final JiraAuthenticationContext context, final OrganisationService organisationService)
    {
        this.organisationService = organisationService;
        this.store = store;
        this.jiraContext = context;
    }

    public String doDefault() throws java.lang.Exception
    {
        setOrganisation(store.getOrganisationById(organisationId));
        return super.doExecute();
    }

    public String doExecute() throws Exception
    {
        setOrganisation(store.getOrganisationById(organisationId));

        return super.doExecute();
    }

    public boolean allowJoin()
    {
        //if(organisation.getVerified())
        {
            User currentUser = jiraContext.getLoggedInUser();
            if (currentUser != null)
            {
                if (!getMembers().contains(currentUser.getName()))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean noMembers()
    {
        return (getMembers() == null || getMembers().isEmpty());
    }

    public void setOrganisation(Organisation organisation)
    {
        this.organisation = organisation;
    }

    public Organisation getOrganisation()
    {
        return this.organisation;
    }

    public void setOrganisationId(int organisationId)
    {
        this.organisationId = organisationId;
    }

    private Collection<User> getMembers()
    {
        if (members == null)
        {
            members = organisationService.getMembersOfOrganisation(organisation);
        }
        return members;
    }
}
