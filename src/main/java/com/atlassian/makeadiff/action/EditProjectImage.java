package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.makeadiff.api.ProjectImageService;

/**
 * Webwork action for uploading a project image.
 */
public class EditProjectImage extends JiraWebActionSupport
{
    private final FoundationProjectService foundationProjectService;
    private final ProjectImageService projectImageService;

    private String key;
    private FoundationProject project;

    public EditProjectImage(final FoundationProjectService foundationProjectService, final ProjectImageService projectImageService)
    {
        this.foundationProjectService = foundationProjectService;
        this.projectImageService = projectImageService;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (getProject() == null)
        {
            return "notexist";
        }

        if (!foundationProjectService.isAllowedToEdit(getLoggedInApplicationUser(), getProject()))
        {
            return "nopermission";
        }

        return INPUT;
    }

    public FoundationProject getProject()
    {
        if (project == null)
        {
            project = foundationProjectService.getProject(key);
        }

        return project;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public boolean isHasProjectImage()
    {
        return projectImageService.hasBannerImage(key);
    }
}
