package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * Webwork action for viewing the login up page.
 */
public class FoundationLogin extends JiraWebActionSupport
{
    public static final String ALREADYLOGGEDIN = "alreadyloggedin";

    @Override
    public String doDefault() throws Exception
    {
        return getLoggedInApplicationUser() != null ?
                ALREADYLOGGEDIN :
                super.doDefault();
    }
}

