package com.atlassian.makeadiff.action;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Webwork action for the home page.
 */
public class FoundationHome extends JiraWebActionSupport
{
    private final FoundationProjectService projectService;
    private final UserManager userManager;
    private final AvatarService avatarService;
    private final TaskService taskService;

    private List<FoundationProject> myProjects;
    private List<FoundationProject> remainingProjects;

    public FoundationHome(final FoundationProjectService projectService, final UserManager userManager, final AvatarService avatarService, final TaskService taskService)
    {
        this.projectService = projectService;
        this.userManager = userManager;
        this.avatarService = avatarService;
        this.taskService = taskService;
    }

    @Override
    protected String doExecute() throws Exception
    {
        myProjects = new ArrayList<FoundationProject>();
        remainingProjects = new ArrayList<FoundationProject>();

        final List<FoundationProject> projects = projectService.getAllProjects(getLoggedInApplicationUser());
        for (final FoundationProject project : projects)
        {
            if (isContributor(project))
            {
                myProjects.add(project);
            }
            else
            {
                remainingProjects.add(project);
            }
        }

        return returnComplete();
    }

    public List<FoundationProject> getMyProjects()
    {
        return myProjects;
    }

    public List<FoundationProject> getRemainingProjects()
    {
        return remainingProjects;
    }

    public String getStatusLabel(ProjectStatus status)
    {
       return status.getLabel(getLoggedInApplicationUser());
    }

    public int getToDoTasksCount(final String projectKey)
    {
        return taskService.getToDoTasksCount(projectKey);
    }

    public int getInProgressTasksCount(final String projectKey)
    {
        return taskService.getInProgressTasksCount(projectKey);
    }

    public int getDoneTasksCount(final String projectKey)
    {
        return taskService.getDoneTasksCount(projectKey);
    }

    public URI getAvatarUrl(final String userKey)
    {
        return getAvatarUrl(userKey, Avatar.Size.SMALL);
    }

    public URI getMediumAvatarUrl(final String userKey)
    {
        return getAvatarUrl(userKey, Avatar.Size.MEDIUM);
    }

    private URI getAvatarUrl(final String userKey, Avatar.Size size)
    {
        ApplicationUser targetUser = userManager.getUserByKey(userKey);
        return avatarService.getAvatarURL(getLoggedInApplicationUser(), targetUser, size);
    }

    private boolean isContributor(final FoundationProject project)
    {
        final ApplicationUser user = getLoggedInApplicationUser();
        if (user == null)
        {
            return false;
        }

        for (final FoundationProjectContributor contributor : projectService.getAllContributorsForProject(project))
        {
            if (contributor.getUser().getKey().equals(user.getKey()))
            {
                return true;
            }
        }

        return false;
    }
}
