package com.atlassian.makeadiff.action;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.exception.VelocityException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * Webwork action for the user profile page.
 */
public class UserProfileAction extends JiraWebActionSupport
{
    private static final String DEFAULT_TAB = "current";

    private String userKey;
    private String selectedTab;

    private UserProfile userProfile;
    private final UserProfileService userProfileService;
    private final TaskService taskService;
    private final FoundationProjectService foundationProjectService;
    private final VelocityTemplatingEngine velocityTemplatingEngine;
    private final JiraAuthenticationContext authenticationContext;
    private final AvatarManager avatarManager;

    public UserProfileAction(final UserProfileService userProfileService,
                             final TaskService taskService,
                             final FoundationProjectService foundationProjectService,
                             final VelocityTemplatingEngine velocityTemplatingEngine,
                             final JiraAuthenticationContext authenticationContext,
                             final AvatarManager avatarManager
                         )
    {
        this.userProfileService = userProfileService;
        this.taskService = taskService;
        this.foundationProjectService = foundationProjectService;
        this.velocityTemplatingEngine = velocityTemplatingEngine;
        this.authenticationContext = authenticationContext;
        this.avatarManager = avatarManager;
    }

    public String getUserKey()
    {
        if (userKey == null)
        {
            userKey = getLoggedInApplicationUser().getKey();
        }
        return userKey;
    }

    public void setUserKey(String userKey)
    {
        this.userKey = userKey.toLowerCase();
    }

    public String getUserAvatarUrl()
    {
        return userProfileService.getAvatarURL(getUserProfile(), Avatar.Size.LARGE).toString();
    }

    protected String doExecute() throws Exception
    {
        return (getLoggedInApplicationUser() == null) ? "notloggedin" : SUCCESS;
    }

    public String getUsername()
    {
        return (getUserProfile() == null) ? null : getUserProfile().getUsername();
    }

    public String getDisplayName()
    {
        return (getUserProfile() == null) ? null : getUserProfile().getDisplayName();
    }

    public String getFirstName()
    {
        return (getDisplayName() == null) ? null : getDisplayName().split(" ")[0];
    }

    public UserProfile getUserProfile()
    {
        if (userProfile == null)
        {
            String userKey = getUserKey();
            if (userKey == null)
                return null;

            userProfile = userProfileService.getUserProfile(userKey);
        }
        return userProfile;
    }

    public List<Issue> getUserResolvedIssues()
    {
        List<Issue> issues = taskService.getResolvedTasksForUser(getUserProfile());
        System.out.println(issues);
        return issues;
    }

    public List<FoundationProject> getUserCurrentProjects()
    {
        Map<Project, List<Issue>> projectIssuesMap = getProjectIssuesMap();
        List<FoundationProject> foundationProjects = new ArrayList<FoundationProject>();
        for (Project project : projectIssuesMap.keySet())
        {
            FoundationProject foundationProject = foundationProjectService.getProject(project.getKey());
            foundationProjects.add(foundationProject);
        }

        return foundationProjects;
    }

    private Map<Project, List<Issue>> getProjectIssuesMap()
    {
        List<Issue> issues = getUserResolvedIssues();
        Map<Project, List<Issue>> projectMap = new HashMap<Project, List<Issue>>();

        for (Issue issue : issues)
        {
            Project issueProject = issue.getProjectObject();
            List<Issue> projectIssues = projectMap.get(issueProject);
            if (projectIssues == null)
            {
                projectIssues = new ArrayList<Issue>();
                projectMap.put(issueProject, projectIssues);
            }
            projectIssues.add(issue);
        }

        return projectMap;
    }

    public String getTabContentWithHtml()
    {
        final Map<String, Object> velocityParams = createVelocityParams();

        try
        {
            return velocityTemplatingEngine.render(file("templates/user-profile-tab-" + getSelectedTab() + "-projects.vm")).applying(velocityParams).asHtml();
        }
        catch (VelocityException e)
        {
            log.error(e);
            return "";
        }
    }

    public String getSelectedTab()
    {
        return (StringUtils.isBlank(selectedTab)) ? DEFAULT_TAB : selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    private Map<String, Object> createVelocityParams()
    {
        final Map<String, Object> velocityParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
        velocityParams.put("action", this);
        return velocityParams;

    }

    public boolean canEdit() {
        return getLoggedInApplicationUser().getKey().equals(getUserKey());
    }

    public Long getDefaultAvatarId()
    {
        return avatarManager.getDefaultAvatarId(Avatar.Type.USER);
    }
}
