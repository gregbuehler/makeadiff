package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.OrganisationStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 23/03/13
 * Time: 7:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class VerifyCharity extends JiraWebActionSupport
{
    private final OrganisationService organisationService;
    private final OrganisationStore store;

    public VerifyCharity(OrganisationService organisationService, OrganisationStore store)
    {
        this.organisationService = organisationService;
        this.store = store;
    }

    public List<Organisation> getUnverifiedOrganisation()
    {

        List<Organisation> listToReturn = new ArrayList<Organisation>();
        for (Organisation org : store.getAllOrganisations())
        {
            // TODO: If the org is not already verified, add it to the listToReturn
            if (!org.getVerified())
            {
                listToReturn.add(org);
            }
        }
        return listToReturn;
    }

}
