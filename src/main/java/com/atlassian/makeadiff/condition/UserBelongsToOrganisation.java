package com.atlassian.makeadiff.condition;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.makeadiff.api.UserProfile;
import com.atlassian.makeadiff.api.UserProfileService;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class UserBelongsToOrganisation implements Condition
{
    private final JiraAuthenticationContext authenticationContext;
    private final UserProfileService userProfileService;

    public UserBelongsToOrganisation(final JiraAuthenticationContext authenticationContext, final UserProfileService userProfileService)
    {
        this.authenticationContext = authenticationContext;
        this.userProfileService = userProfileService;
    }

    @Override
    public void init(final Map<String, String> context) throws PluginParseException
    {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context)
    {
        final ApplicationUser user = authenticationContext.getUser();
        if (user == null)
        {
            return false;
        }

        final UserProfile userProfile = userProfileService.getUserProfile(user.getKey());
        if (userProfile == null)
        {
            return false;
        }

        return userProfile.belongsToAnyOrganisations();
    }
}
