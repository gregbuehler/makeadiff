package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to show the about page.
 */
public class AboutPageFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        request.getRequestDispatcher(request.getContextPath() + "/secure/About.jspa").forward(request, response);
    }
}