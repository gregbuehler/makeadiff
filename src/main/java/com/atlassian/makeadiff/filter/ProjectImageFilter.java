package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.makeadiff.api.ProjectImageService;
import com.atlassian.makeadiff.api.ProjectImageSize;
import org.apache.commons.lang.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Serves project images.
 */
public class ProjectImageFilter extends AbstractHttpFilter
{
    private final ProjectImageService projectImageService;

    public ProjectImageFilter(final ProjectImageService projectImageService)
    {
        this.projectImageService = projectImageService;
    }

    @Override
    public void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws IOException, ServletException
    {
        final String projectKey = getProjectKey(request);
        final ProjectImageSize size = getSize(request);
        if (StringUtils.isBlank(projectKey))
        {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Missing argument: projectKey");
        }

        boolean bytesWritten = false;
        try
        {
            final File imageFile = projectImageService.getBannerImage(projectKey, size);
            if (imageFile == null)
            {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Project image not found");
            }
            else
            {
                ImageFilterHelper.sendImage(response, imageFile);
                bytesWritten = true;
            }
        }
        catch (IOException e)
        {
            // TODO
            //handleOutputStreamingException(response, bytesWritten, e);
        }
        catch (RuntimeException e)
        {
            // TODO
            //handleOutputStreamingException(response, bytesWritten, e);
        }
    }

    private String getProjectKey(final HttpServletRequest request)
    {
        return StringUtils.trim(request.getParameter("projectKey"));
    }

    private ProjectImageSize getSize(final HttpServletRequest request)
    {
        final String size = StringUtils.trim(request.getParameter("size"));
        return ProjectImageSize.getFromName(size);
    }
}
