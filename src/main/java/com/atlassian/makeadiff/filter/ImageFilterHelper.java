package com.atlassian.makeadiff.filter;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.util.StreamCopyingConsumer;
import com.atlassian.jira.web.servlet.HttpResponseHeaders;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class ImageFilterHelper
{
    private static final int IMAGE_BUFFER_SIZE = 4096;

    public static void sendImage(final HttpServletResponse response, final File imageFile)
            throws IOException
    {
        response.setContentType(AvatarManager.AVATAR_IMAGE_FORMAT_FULL.getContentType());
        HttpResponseHeaders.cachePrivatelyForAboutOneYear(response);
        final OutputStream out = response.getOutputStream();
        StreamCopyingConsumer streamCopier = new StreamCopyingConsumer(out, IMAGE_BUFFER_SIZE);

        final InputStream data = new FileInputStream(imageFile);
        try
        {
            streamCopier.consume(data);
        }
        finally
        {
            data.close();
        }
    }
}
