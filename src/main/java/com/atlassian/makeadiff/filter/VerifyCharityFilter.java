package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 23/03/13
 * Time: 7:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class VerifyCharityFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        request.getRequestDispatcher(request.getContextPath() + "/secure/VerifyCharity.jspa").forward(request, response);
    }
}
