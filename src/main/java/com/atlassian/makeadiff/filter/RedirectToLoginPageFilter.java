package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to redirect to the custom sign up page.
 */
public class RedirectToLoginPageFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        if (request.getMethod().equals("GET"))
            response.sendRedirect(request.getContextPath() + "/view/login");
        else
            //response.sendRedirect(request.getContextPath() + "/view/home");
            filterChain.doFilter(request, response);
    }
}