package com.atlassian.makeadiff.listener;

import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.user.UserProfileUpdatedEvent;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.util.UserManager;

/**
 * Keep usernames in sync with email addresses.
 */
public class SyncUsernameAndEmailListener
{
    private final UserManager userManager;

    public SyncUsernameAndEmailListener(EventPublisher eventPublisher, UserManager userManager)
    {
        eventPublisher.register(this);
        this.userManager = userManager;
    }

    @EventListener
    public void onUserProfileUpdated(UserProfileUpdatedEvent event)
    {
        syncUserAndEmailAddress(event.getUsername());
    }

    @EventListener
    public void onUserCreated(UserCreatedEvent event)
    {
        syncUserAndEmailAddress(event.getUser().getName());
    }

    private void syncUserAndEmailAddress(String username)
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            throw new IllegalStateException("Unable to find updated user: " + username);
        }

        // if the username is out of sync with the email address, update it
        if (!user.getUsername().equals(user.getEmailAddress()))
        {
            ImmutableUser.Builder builder = ImmutableUser.newUser(user.getDirectoryUser());
            builder.name(user.getEmailAddress());
            ApplicationUser updatedUser = new DelegatingApplicationUser(user.getKey(), builder.toUser());
            userManager.updateUser(updatedUser);
        }
    }
}
