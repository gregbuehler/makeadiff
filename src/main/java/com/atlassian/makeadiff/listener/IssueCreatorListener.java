package com.atlassian.makeadiff.listener;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.makeadiff.api.ProjectStatus;


//The IssueCreatorListener listens for the creation of a task from the add task form.
//It then updates the project status to In Progress if it is not already at that stage.
public class IssueCreatorListener
{

    private final FoundationProjectService projectService;
    private final JiraAuthenticationContext authContext;

    public IssueCreatorListener(EventPublisher eventPublisher, FoundationProjectService projectService, JiraAuthenticationContext authContext)
    {
        eventPublisher.register(this);
        this.projectService = projectService;
        this.authContext = authContext;
    }

    @EventListener
    public void onIssueCreation(IssueEvent event)
    {
        if (event.getEventTypeId().equals(EventType.ISSUE_CREATED_ID) && event.getProject().getProjectLead().getEmailAddress().equals(authContext.getLoggedInUser().getEmailAddress()))
        {
            FoundationProject project = projectService.getProject(event.getProject().getKey());
            // Get project from storage
            final FoundationProjectEntity entity = new FoundationProjectEntity();
            entity.setKey(project.getKey());
            entity.setCreatorUserKey(project.getCreator().getKey());
            entity.setStatusKey(project.getStatus().getStorageKey());
            entity.setLocation(project.getLocation());
            entity.setRepo(project.getRepo());

            if (project.getStatus().equals(ProjectStatus.NEW))
            {
                project.setStatus(ProjectStatus.IN_PROGRESS);
                entity.setStatusKey(ProjectStatus.IN_PROGRESS.getStorageKey());
            }
            FoundationProjectService.ValidationResult validationResult = projectService.validateCreateProject(project.getName(), project.getDescription(), entity, project.getUrl(), project.getLocation(), project.getRepo());
            projectService.updateProject(project, validationResult);

        }
    }

    @EventListener
    public void onIssueUpdate(IssueEvent event)
    {
        if (event.getEventTypeId().equals(EventType.ISSUE_UPDATED_ID) && event.getProject().getProjectLead().equals(authContext.getLoggedInUser()))
        {
            //TODO: Add code to update the project status ONLY if the user trying to change it is authorised
//            if (projectService.getProject(event.getProject().getKey()).getStatus())
        }
    }

}