package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

/**
 * Entity to represent a "Make a Diff" project.
 */
public class FoundationProject implements Project
{
    private static final Logger log = LoggerFactory.getLogger(FoundationProject.class);

    private final Project project;
    private final FoundationProjectEntity entity;
    private final FoundationProjectService projectService;
    private final TaskService taskService;

    // Cached values
    private ApplicationUser creator;
    private List<FoundationProjectContributor> contributors;
    private ProjectStatus status;

    /**
     * Do not construct instances directly, use {@link FoundationProjectService#convertToFoundationProjectEntity(com.atlassian.jira.project.Project)}
     */
    FoundationProject(final Project project, final FoundationProjectEntity entity, final FoundationProjectService projectService, final TaskService taskService)
    {
        this.project = project;
        this.entity = entity;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public FoundationProjectEntity getEntity()
    {
        return entity;
    }

    public List<FoundationProjectContributor> getAllContributors()
    {
        if (contributors == null)
        {
            contributors = projectService.getAllContributorsForProject(this);
        }
        return contributors;
    }

    public ApplicationUser getCreator()
    {
        if (creator == null)
        {
            creator = ComponentAccessor.getUserManager().getUserByKey(entity.getCreatorUserKey());
        }

        return creator;
    }

    public ProjectStatus getStatus()
    {
        if (status == null)
        {
            status = ProjectStatus.findByStorageKey(entity.getStatusKey());
        }

        return status;
    }

    public boolean isNew()
    {
        return getStatus() == ProjectStatus.NEW;
    }

    public boolean isInProgress()
    {
        return getStatus() == ProjectStatus.IN_PROGRESS;
    }

    public boolean isCompleted()
    {
        return getStatus() == ProjectStatus.COMPLETED;
    }

    public List<Issue> getTasks()
    {
        return taskService.getIssues(project.getKey());
    }

    public Long getAgileBoardId()
    {
        return entity.getAgileBoardId();
    }

    @Override
    public Long getId()
    {
        return project.getId();
    }

    @Override
    public String getName()
    {
        return project.getName();
    }

    @Override
    public String getKey()
    {
        return project.getKey();
    }

    @Override
    public String getUrl()
    {
        return project.getUrl();
    }


    @Override
    public String getEmail()
    {
        return project.getEmail();
    }

    @Override
    public User getLead()
    {
        return project.getLead();
    }

    @Override
    public User getLeadUser()
    {
        return project.getLeadUser();
    }

    @Override
    public String getLeadUserName()
    {
        return project.getLeadUserName();
    }

    public String getDescription()
    {
        return project.getDescription();
    }

    @Override
    public Long getAssigneeType()
    {
        return project.getAssigneeType();
    }

    @Override
    public Long getCounter()
    {
        return project.getCounter();
    }

    @Override
    public Collection<GenericValue> getComponents()
    {
        return project.getComponents();
    }

    @Override
    public Collection<ProjectComponent> getProjectComponents()
    {
        return project.getProjectComponents();
    }

    @Override
    public Collection<Version> getVersions()
    {
        return project.getVersions();
    }

    @Override
    public Collection<IssueType> getIssueTypes()
    {
        return project.getIssueTypes();
    }

    @Override
    public GenericValue getProjectCategory()
    {
        return project.getProjectCategory();
    }

    @Override
    public ProjectCategory getProjectCategoryObject()
    {
        return project.getProjectCategoryObject();
    }

    @Override
    public GenericValue getGenericValue()
    {
        return project.getGenericValue();
    }

    @Override
    public Avatar getAvatar()
    {
        return project.getAvatar();
    }

    @Override
    public ApplicationUser getProjectLead()
    {
        return project.getProjectLead();
    }

    @Override
    public String getLeadUserKey()
    {
        return project.getLeadUserKey();
    }

    @Override
    public String getOriginalKey()
    {
        return project.getOriginalKey();
    }

    public void setStatus(ProjectStatus status)
    {
        this.entity.setStatusKey(status.getStorageKey());
    }

    public String getLocation()
    {
        return entity.getLocation();
    }

    public void setLocation(String location)
    {
        entity.setLocation(location);
    }

    public String getRepo()
    {
        return entity.getRepo();
    }

    public void setRepo(String repo)
    {
        entity.setRepo(repo);
    }

}
