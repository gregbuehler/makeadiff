package com.atlassian.makeadiff.api;

/**
 * Data access layer for {@link UserProfile}s.
 */
public interface UserProfileStore
{
    UserProfile getUserProfile(String key);

    void createUserProfile(String key, String skills, String description);

    void updateUserProfile(UserProfile userProfile);
}
