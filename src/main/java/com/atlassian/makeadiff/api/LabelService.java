package com.atlassian.makeadiff.api;

import com.atlassian.jira.project.Project;

import java.util.Map;

public interface LabelService
{
    Map<String, Long> getAllLabelsInProject(Project project);
}
