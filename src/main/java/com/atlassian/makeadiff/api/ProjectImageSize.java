package com.atlassian.makeadiff.api;

public enum ProjectImageSize
{
    FULL("full", 0, 0),
    BIG("big", 637, 0),
    SMALL("small", 310, 200);

    private final String name;
    private final int width;
    private final int height;

    private ProjectImageSize(final String name, final int width, final int height)
    {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName()
    {
        return name;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public static ProjectImageSize getFromName(final String name)
    {
        for (final ProjectImageSize size : ProjectImageSize.values())
        {
            if (size.getName().equals(name))
            {
                return size;
            }
        }

        return null;
    }
}
