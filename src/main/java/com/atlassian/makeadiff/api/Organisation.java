package com.atlassian.makeadiff.api;


/**
 * Represents a charity or not-for-profit organisation.
 */
public class Organisation
{
    private final int id;
    private final String key;
    private final String name;
    private final String url;
    private final String description;
    private final boolean verified;

    public Organisation(final int id, final String key, final String name, final String url, final String description, final boolean verified)
    {
        this.id = id;
        this.key = key;
        this.name = name;
        this.url = url;
        this.description = description;
        this.verified = verified;
    }

    public int getId()
    {
        return id;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean getVerified()
    {
        return verified;
    }
}
