package com.atlassian.makeadiff.api.workflow;

/**
 * Contains the information required to create a workflow status.
 */
public enum WorkflowStatus
{
    TODO("To Do", "", "/images/icons/statuses/open.png"),
    IN_PROGRESS("In Progress", "", "/images/icons/statuses/inprogress.png"),
    DONE("Done", "", "/images/icons/statuses/closed.png");

    private final String name;
    private final String description;
    private final String iconUrl;

    WorkflowStatus(final String name, final String description, final String iconUrl)
    {
        this.name = name;
        this.description = description;
        this.iconUrl = iconUrl;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }
}