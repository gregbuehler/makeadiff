package com.atlassian.makeadiff.api.workflow;

import com.atlassian.jira.config.ResolutionManager;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;

import java.util.Collection;

/**
 * Copied from com.atlassian.greenhopper.service.workflow.WorkflowConstantsServiceImpl.
 */
public class WorkflowConstantsService
{
    private final StatusManager statusManager;
    private final ResolutionManager resolutionManager;

    public WorkflowConstantsService(final StatusManager statusManager, final ResolutionManager resolutionManager)
    {
        this.statusManager = statusManager;
        this.resolutionManager = resolutionManager;
    }

    public Status getOrCreateStatus(String name, String description, String iconUrl)
    {
        Status status = getStatusByName(name);
        if (status == null)
        {
            status = statusManager.createStatus(name, description, iconUrl);
        }
        return status;
    }

    public Resolution getOrCreateResolution(String name)
    {
        Resolution resolution = getResolutionByName(name);
        if (resolution == null)
        {
            resolution = resolutionManager.createResolution(name, "");
        }
        return resolution;
    }

    public Status getStatusByName(String name)
    {
        Collection<Status> statuses = statusManager.getStatuses();
        for (Status status : statuses)
        {
            if (status.getName().equalsIgnoreCase(name))
            {
                return status;
            }
        }
        return null;
    }

    public Resolution getResolutionByName(String name)
    {
        return resolutionManager.getResolutionByName(name);
    }
}