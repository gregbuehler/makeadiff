package com.atlassian.makeadiff.api;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Functions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Creates a project key
 */
public class GenerateProjectKeyService
{
    // ------------------------------------------------------------------------------------------------------- Constants

    // These words will not be used in key generation for acronyms.
    private final List<String> IGNORED_WORDS = Lists.newArrayList("A", "IS", "AN", "AS", "AND", "OF", "OR");
    // The (non-ascii) characters used as keys will be replaced with their (ascii) value.
    private static final Map<Integer, String> CHARACTER_MAP = new HashMap<Integer, String>();

    static
    {
        CHARACTER_MAP.put(199, "C"); // �
        CHARACTER_MAP.put(231, "c"); // ?
        CHARACTER_MAP.put(252, "u"); // �
        CHARACTER_MAP.put(251, "u"); // ?
        CHARACTER_MAP.put(250, "u"); // �
        CHARACTER_MAP.put(249, "u"); // ?
        CHARACTER_MAP.put(233, "e"); // ?
        CHARACTER_MAP.put(234, "e"); // ?
        CHARACTER_MAP.put(235, "e"); // �
        CHARACTER_MAP.put(232, "e"); // ?
        CHARACTER_MAP.put(226, "a"); // �
        CHARACTER_MAP.put(228, "a"); // ?
        CHARACTER_MAP.put(224, "a"); // �
        CHARACTER_MAP.put(229, "a"); // �
        CHARACTER_MAP.put(225, "a"); // �
        CHARACTER_MAP.put(239, "i"); // �
        CHARACTER_MAP.put(238, "i"); // �
        CHARACTER_MAP.put(236, "i"); // �
        CHARACTER_MAP.put(237, "i"); // �
        CHARACTER_MAP.put(196, "A"); // �
        CHARACTER_MAP.put(197, "A"); // ?
        CHARACTER_MAP.put(201, "E"); // �
        CHARACTER_MAP.put(230, "ae"); // ?
        CHARACTER_MAP.put(198, "Ae"); // �
        CHARACTER_MAP.put(244, "o"); // �
        CHARACTER_MAP.put(246, "o"); // ?
        CHARACTER_MAP.put(242, "o"); // �
        CHARACTER_MAP.put(243, "o"); // �
        CHARACTER_MAP.put(220, "U"); // �
        CHARACTER_MAP.put(255, "Y"); // �
        CHARACTER_MAP.put(214, "O"); // �
        CHARACTER_MAP.put(241, "n"); // �
        CHARACTER_MAP.put(209, "N"); // �

    }

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProjectManager projectManager;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public GenerateProjectKeyService(ProjectManager projectManager)
    {
        this.projectManager = projectManager;
    }

    // For test only
    GenerateProjectKeyService()
    {
        this.projectManager = null;
    }

    // -------------------------------------------------------------------------------------------------- Public Methods
    public String generateUniqueKey(String name)
    {
        String candidateKey = generateKey(name);

        // Ideally we'd query here...
        boolean matchExisting = false;
        List<String> similarKeys = Lists.newArrayList();
        List<Project> projects = projectManager.getProjectObjects();
        for (Project project : projects)
        {
            String key = project.getKey();
            if (key.startsWith(candidateKey))
            {
                similarKeys.add(key);
            }

            if (key.equals(candidateKey))
            {
                matchExisting = true;
            }
        }

        if (!matchExisting)
        {
            return candidateKey;
        }
        else
        {
            // Loop through it all
            int i = 1;
            String newKey = candidateKey + i;
            while (true)
            {
                if (similarKeys.contains(newKey))
                {
                    i++;
                    newKey = candidateKey + i;
                }
                else
                {
                    return newKey;
                }
            }
        }
    }

    public String generateKey(String name)
    {
        return generateKey(name, 4, 20);
    }

    public String generateKey(String name, int desiredKeyLength, int maxKeyLength)
    {
        name = StringUtils.trimToNull(name);
        if (name == null)
        {
            throw new IllegalArgumentException("Name is empty or null");
        }

        if (name.length() < 2)
        {
            throw new IllegalArgumentException("Name must be at least 2 characters");
        }

        // Brute-force chunk-by-chunk substitution and filtering, uppercase,
        StringBuilder sb = new StringBuilder();
        char[] chars = name.toCharArray();
        for (char c : chars)
        {
            // Drop crappy chars
            if (Character.isLetterOrDigit(c) || Character.isWhitespace(c))
            {
                if (CHARACTER_MAP.containsKey(Character.getNumericValue(c)))
                {
                    String newChar = CHARACTER_MAP.get(Character.getNumericValue(c));
                    sb.append(newChar);
                }
                else
                {
                    sb.append(c);
                }
            }
        }

        name = sb.toString().toUpperCase();


        // Split into words
        Collection<String> words = Collections2.filter(Lists.newArrayList(StringUtils.split(name)), new Predicate<String>()
        {
            @Override
            public boolean apply(@Nullable String s)
            {
                return !IGNORED_WORDS.contains(s);
            }
        });


        String key;

        if (words.isEmpty())
        {
            // No words were worthy!
            key = "";
        }
        else if (words.size() == 1)
        {
            // If we have one word, and it is longer than a desired key, get the first syllable
            String word = words.iterator().next();
            String firstSyllable = getFirstSyllable(word);

            if (maxKeyLength > 0 && maxKeyLength < word.length() || // must use the smaller one
                    Math.abs(word.length() - desiredKeyLength) >= Math.abs(firstSyllable.length() - desiredKeyLength))
            {
                // should use the smaller one because it's closer to desired length.
                key = firstSyllable;
            }
            else
            {
                key = word;
            }
        }
        else
        {
            int totalLength = getTotalLength(words);
            String acronym = createAcronym(words);
            if (maxKeyLength < totalLength || // must use the smaller one
                    Math.abs(totalLength - desiredKeyLength) >= Math.abs(acronym.length() - desiredKeyLength))
            { // should use the smaller one because it's closer to desired length.
                key = acronym;
            }
            else
            {
                // The words are short enough to use as a key
                key = StringUtils.join(words);
            }
        }

        // Limit the length of the key
        if (maxKeyLength > 0 && key.length() > maxKeyLength)
        {
            key = key.substring(0, maxKeyLength);
        }

        if (StringUtils.isNumeric(key))
        {
            throw new IllegalArgumentException("Name must contain non numeric characters");
        }

        return key;
    }

    // -------------------------------------------------------------------------------------------------- Helper Methods

    private int getTotalLength(Iterable<String> words)
    {
        return StringUtils.join(words).length();
    }

    private String createAcronym(Iterable<String> words)
    {
        return Functions.fold(new Function2<String, String, String>()
        {
            @Override
            public String apply(String arg1, String arg2)
            {
                return arg1 + (StringUtils.defaultString(StringUtils.left(arg2, 1)));
            }
        }, "", words);
    }

    private String getFirstSyllable(String word)
    {
        // Best guess at getting the first syllable
        // Returns the substring up to and including the first consonant to appear after a vowel
        boolean pastVowel = false;

        char[] chars = word.toCharArray();


        int i;
        for (i = 0; i < chars.length; i++)
        {
            if (isVowelOrY(chars[i]))
            {
                pastVowel = true;
            }
            else
            {
                if (pastVowel)
                {
                    return word.substring(0, i + 1);
                }
            }
        }
        return word;
    }

    private boolean isVowelOrY(char c)
    {
        return "AEIOUY".indexOf(c) != -1;
    }
}
