package com.atlassian.makeadiff.api;

import java.io.InputStream;

/**
 * A simple class to hold image details.
 */
public class ImageDescriptor
{
    private final InputStream inputStream;
    private final String contentType;
    private final String filename;

    public ImageDescriptor(final InputStream inputStream, final String contentType, final String filename)
    {
        this.inputStream = inputStream;
        this.contentType = contentType;
        this.filename = filename;
    }

    public InputStream getInputStream()
    {
        return inputStream;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getFilename()
    {
        return filename;
    }
}
