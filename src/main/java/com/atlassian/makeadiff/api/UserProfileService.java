package com.atlassian.makeadiff.api;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import java.net.URI;

/**
 * Service to get, edit and create user profiles.
 */
public class UserProfileService
{
    private final UserProfileStore store;
    private final AvatarService avatarService;
    private final JiraAuthenticationContext authContext;
    private final UserManager userManager;

    public UserProfileService(final UserProfileStore store, final AvatarService avatarService,
                              final JiraAuthenticationContext authContext, final UserManager userManager)
    {
        this.store = store;
        this.avatarService = avatarService;
        this.authContext = authContext;
        this.userManager = userManager;
    }

    public UserProfile getUserProfile(final String key)
    {
        return store.getUserProfile(key);
    }

    public void createUserProfile(final String key, final String skills, final String description)
    {
        store.createUserProfile(key, skills, description);
    }

    public void updateUserProfile(UserProfile userProfile)
    {
        store.updateUserProfile(userProfile);
    }

    // This kind of doesn't belong here, since it doesn't strictly relate to CRUD operations on the UserProfile entity.
    // However, displaying an Avatar alongside user profile information seems like a pretty common requirement, so an
    // easy way to map from UserProfile -> Avatar is handy.
    public URI getAvatarURL(UserProfile userProfile, Avatar.Size size)
    {
        ApplicationUser targetUser = userManager.getUserByKey(userProfile.getKey());
        return avatarService.getAvatarURL(authContext.getUser(), targetUser, size);
    }
}
