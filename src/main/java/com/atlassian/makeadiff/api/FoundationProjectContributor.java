package com.atlassian.makeadiff.api;

import com.atlassian.jira.user.ApplicationUser;

import java.net.URI;

/**
 * Provides a mapping between instances of {@link FoundationProject} and the users who have contributed to them.
 */
public class FoundationProjectContributor
{
    /**
     * Identifies the main type of contribution that the user has made to the project.
     */
    public enum ContributorType
    {
        OWNER(1, "makeadiff.project.sidebar.creator"),
        CHAMPION(2, "makeadiff.project.sidebar.champion"),
        CONTRIBUTOR(3, "makeadiff.project.sidebar.contributor");

        private final int order;
        private final String i18nKey;

        ContributorType(int order, String i18nKey)
        {
            this.order = order;
            this.i18nKey = i18nKey;
        }

        public int getOrder()
        {
            return order;
        }

        public String getI18nKey()
        {
            return i18nKey;
        }
    }

    private final FoundationProject project;
    private final ApplicationUser user;
    private final UserProfile userProfile;
    private final ContributorType contributorType;
    private final URI avatarURL;
    private final int contributions;

    /**
     * Get at the contributors by calling {@link FoundationProject#getAllContributors()}.
     *
     * @param project         The {@link FoundationProject} that the user contributed to.
     * @param user            The {@link ApplicationUser} who contributed to the project.
     * @param contributorType Identifies the main style of contribution that the user made.
     * @param userProfile     A reference to the user's {@link UserProfile}.
     * @param contributions   An aggregate of the number of contributions that the user has made to the project.
     * @param avatarURL       A link to the user's avatar image.
     */
    FoundationProjectContributor(final FoundationProject project, final ApplicationUser user,
                                 final ContributorType contributorType, final UserProfile userProfile,
                                 final int contributions, final URI avatarURL)
    {
        this.project = project;
        this.user = user;
        this.userProfile = userProfile;
        this.contributorType = contributorType;
        this.contributions = contributions;
        this.avatarURL = avatarURL;
    }

    public FoundationProject getProject()
    {
        return project;
    }

    public ApplicationUser getUser()
    {
        return user;
    }

    public UserProfile getUserProfile()
    {
        return userProfile;
    }

    public ContributorType getContributorType()
    {
        return contributorType;
    }

    public int getContributions()
    {
        return contributions;
    }

    public URI getAvatarURL()
    {
        return avatarURL;
    }
}
