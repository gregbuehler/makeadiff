package com.atlassian.makeadiff.api;

import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;


public class PermissionSchemeCreator
{
    private final PermissionSchemeManager permissionSchemeManager;

    public PermissionSchemeCreator(final PermissionSchemeManager permissionSchemeManager)
    {
        this.permissionSchemeManager = permissionSchemeManager;
    }

    public void createPermissionSchemeForProject(final Project project)
    {

        final Scheme scheme = permissionSchemeManager.copyScheme(permissionSchemeManager.getDefaultSchemeObject());
        associateSchemeToProject(project, scheme);
    }

    private void associateSchemeToProject(final Project project, final Scheme scheme)
    {
        permissionSchemeManager.addSchemeToProject(project, scheme);
    }
}
