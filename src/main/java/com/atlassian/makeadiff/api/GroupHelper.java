package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;

/**
 * A helper class for dealing with user groups.
 */
public class GroupHelper
{
    private static final String ATLASSIAN_GROUP_NAME = "atlassian-staff";

    private final UserUtil userUtil;
    private final GroupManager groupManager;

    public GroupHelper(final UserUtil userUtil, final GroupManager groupManager)
    {
        this.userUtil = userUtil;
        this.groupManager = groupManager;
    }

    public Group getAtlassianGroup()
    {
        return groupManager.getGroup(ATLASSIAN_GROUP_NAME);
    }

    public Group ensureAtlassianGroupExists() throws OperationNotPermittedException, InvalidGroupException
    {
        Group atlassianGroup = getAtlassianGroup();
        if (atlassianGroup == null)
        {
            atlassianGroup = groupManager.createGroup(ATLASSIAN_GROUP_NAME);
        }

        return atlassianGroup;
    }

    public boolean isAtlassianStaff(final User user)
    {
        return user.getEmailAddress().endsWith("@atlassian.com");
    }

    /**
     * If the user is an atlassian staff member, adds them to the atlassian user group. If the atlassian user group does
     * not exist, this method does nothing.
     *
     * @param user
     * @throws AddException
     * @throws PermissionException
     */
    public void addUserToAppropriateGroups(final User user) throws AddException, PermissionException
    {
        final Group atlassianGroup = getAtlassianGroup();
        if (atlassianGroup != null && isAtlassianStaff(user))
        {
            userUtil.addUserToGroup(atlassianGroup, user);
        }
    }

    /**
     * Adds all users with an atlassian email address to the atlassian user group. If the atlassian user group does not
     * exist, this method does nothing.
     *
     * @throws AddException
     * @throws PermissionException
     */
    public void addAllAtlassianUsersToAtlassianGroup() throws AddException, PermissionException
    {
        final Group atlassianGroup = getAtlassianGroup();
        if (atlassianGroup != null)
        {
            for (final ApplicationUser user : userUtil.getAllApplicationUsers())
            {
                if (isAtlassianStaff(user.getDirectoryUser()))
                {
                    userUtil.addUserToGroup(atlassianGroup, user.getDirectoryUser());
                }
            }
        }
    }
}
