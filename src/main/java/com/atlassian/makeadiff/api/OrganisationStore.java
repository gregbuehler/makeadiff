package com.atlassian.makeadiff.api;

import java.util.Collection;

/**
 * Data access layer for {@link Organisation}s.
 */
public interface OrganisationStore
{
    Organisation getOrganisationByKey(String key);

    Organisation getOrganisationById(final int id);

    Organisation getOrganisationByName(String name);

    Collection<Organisation> findOrganisationByLikeName(String likeName);

    Collection<Organisation> getAllOrganisations();

    Collection<Organisation> getAllOrganisations(int limit);

    Organisation createOrUpdateOrganisation(String name, String url, String description, boolean verified);

    Collection<Organisation> getOrganisationsForUser(String userKey);
}
