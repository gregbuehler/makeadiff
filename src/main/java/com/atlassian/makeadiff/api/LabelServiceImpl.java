package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.PagerFilter;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LabelServiceImpl implements LabelService
{

    private final SearchService searchService;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager;
    private final IssueManager issueManager;

    public LabelServiceImpl(SearchService searchService, JiraAuthenticationContext authenticationContext, final ProjectManager projectManager, IssueManager issueManager)
    {
        this.searchService = searchService;
        this.authenticationContext = authenticationContext;
        this.projectManager = projectManager;
        this.issueManager = issueManager;
    }

    @Override
    public Map<String, Long> getAllLabelsInProject(Project project)
    {

        Map<String, Long> labels = new HashMap<String, Long>();
        List<Issue> issuesFromProject = getAllIssuesFromProject(project, authenticationContext.getUser().getDirectoryUser());
        for (Issue i : issuesFromProject)
        {
            for (Label l : i.getLabels())
            {
                Long aLong = labels.get(l.toString());
                labels.put(l.toString(), aLong == null ? 1 : aLong + 1);
            }
        }
        return labels;
    }

    private List<Issue> getAllIssuesFromProject(Project project, User loggedInUser)
    {
        // User is required to carry out a search
        User user = loggedInUser;

        // search issues

        // The search interface requires JQL clause... so let's build one
        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
        // Our JQL clause is simple project="TUTORIAL"
        com.atlassian.query.Query query = jqlClauseBuilder.project(project.getName()).buildQuery();
        // A page filter is used to provide pagination. Let's use an unlimited filter to
        // to bypass pagination.
        PagerFilter<?> pagerFilter = PagerFilter.getUnlimitedFilter();
        com.atlassian.jira.issue.search.SearchResults searchResults = null;
        try
        {
            // Perform search results
            searchResults = searchService.search(user, query, pagerFilter);
        }
        catch (SearchException e)
        {
            e.printStackTrace();
        }
        // return the results
        return searchResults.getIssues();
    }

    public List<Label> getProjectLabels(Project project) throws GenericEntityException
    {
        List<Label> labels = new ArrayList<Label>();
        for (Long issueID : issueManager.getIssueIdsForProject(project.getId()))
        {
            for (Label l : issueManager.getIssueObject(issueID).getLabels())
            {
                labels.add(l);
            }

        }
        return labels;
    }

    /**
     * This method returns all the labels of all the issues of all the projects.
     * It is an impractical query and is not suitable for production, but is included
     * as a proof of concept for the Skills list on the home page.
     *
     * @return
     * @throws GenericEntityException
     */
    public List<Label> getAllLabels() throws GenericEntityException
    {
        List<Label> labels = new ArrayList<Label>();
        for (Project p : projectManager.getProjectObjects())
        {
            labels.addAll(getProjectLabels(p));
        }
        return labels;
    }

    public Map<String, Long> getAllLabelsWeighted() throws GenericEntityException
    {
        List<Label> labels = getAllLabels();
        Map<String, Long> weightedLabels = new HashMap<String, Long>();
        for (Label l : labels)
        {
            String stringLabel = l.getLabel().trim();
            Long aLong = weightedLabels.get(stringLabel);
            weightedLabels.put(stringLabel, aLong == null ? 1 : aLong + 1);
        }
        return weightedLabels;
    }
}
