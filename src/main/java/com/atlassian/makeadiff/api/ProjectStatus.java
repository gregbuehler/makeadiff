package com.atlassian.makeadiff.api;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * Status indicator for a project.
 */
public enum ProjectStatus
{
    NEW("new", "makeadiff.project.status.new.label"),
    IN_PROGRESS("inprogress", "makeadiff.project.status.inprogress.label"),
    COMPLETED("completed", "makeadiff.project.status.completed.label");

    private final String storageKey;
    private final String i18nKey;

    ProjectStatus(String storageKey, String i18nKey)
    {
        this.storageKey = storageKey;
        this.i18nKey = i18nKey;
    }

    public String getStorageKey()
    {
        return storageKey;
    }

    public String getLabel(final ApplicationUser user)
    {
        return getI18n(user).getText(i18nKey);
    }

    private static I18nHelper getI18n(ApplicationUser user)
    {
        I18nHelper.BeanFactory beanFactory = ComponentAccessor.getComponent(I18nHelper.BeanFactory.class);
        return beanFactory.getInstance(user);
    }

    public static ProjectStatus findByStorageKey(final String storageKey)
    {
        for (final ProjectStatus status : values())
        {
            if (storageKey.equals(status.getStorageKey()))
            {
                return status;
            }
        }

        return null;
    }
}
