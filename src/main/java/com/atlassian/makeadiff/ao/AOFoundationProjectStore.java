package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.makeadiff.api.FoundationProjectStore;

@Transactional
public interface AOFoundationProjectStore extends FoundationProjectStore
{
}
