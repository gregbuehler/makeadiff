package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import com.atlassian.makeadiff.api.FoundationProjectStore;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Active objects implementation of the {@link FoundationProjectStore}.
 */
public class AOFoundationProjectStoreImpl implements AOFoundationProjectStore
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ActiveObjects activeObjects;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public AOFoundationProjectStoreImpl(final ActiveObjects activeObjects)
    {
        this.activeObjects = activeObjects;
    }

    // -------------------------------------------------------------------------------------------------- Public Methods
    @Override
    public FoundationProjectEntity getProject(final String projectKey)
    {
        final AOFoundationProjectEntity[] entities = activeObjects.find(AOFoundationProjectEntity.class, "KEY = ?", projectKey);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return convertFromAO(entities[0]);
    }

    @Override
    public Collection<FoundationProjectEntity> getAllProjects()
    {
        return convertToCollection(activeObjects.find(AOFoundationProjectEntity.class));
    }

    @Override
    public void addProject(FoundationProjectEntity project)
    {
        final AOFoundationProjectEntity to = activeObjects.create(AOFoundationProjectEntity.class);
        copyPropertiesFromEntityToAo(project, to);
        to.save();
    }

    @Override
    public void updateProject(FoundationProjectEntity project)
    {
        final AOFoundationProjectEntity[] entities = activeObjects.find(AOFoundationProjectEntity.class, "KEY = ?", project.getKey());

        // TODO: handle this properly.
        if (entities.length == 0)
        {
            return;
        }

        final AOFoundationProjectEntity aoProject = entities[0];
        copyPropertiesFromEntityToAo(project, aoProject);
        aoProject.save();
    }


    // -------------------------------------------------------------------------------------------------- Helper Methods

    private static FoundationProjectEntity convertFromAO(final AOFoundationProjectEntity from)
    {
        final FoundationProjectEntity to = new FoundationProjectEntity();
        copyPropertiesFromAoToEntity(from, to);

        return to;
    }

    private static Collection<FoundationProjectEntity> convertToCollection(final AOFoundationProjectEntity[] entities)
    {
        final Collection<FoundationProjectEntity> result = new ArrayList<FoundationProjectEntity>(entities.length);

        for (final AOFoundationProjectEntity entity : entities)
        {
            result.add(convertFromAO(entity));
        }

        return result;
    }

    private static void copyPropertiesFromAoToEntity(AOFoundationProjectEntity entity, FoundationProjectEntity project)
    {
        project.setKey(entity.getKey());
        project.setCreatorUserKey(entity.getCreatorUserKey());
        project.setStatusKey(entity.getStatusKey());
        project.setLocation(entity.getLocation());
        project.setRepo(entity.getRepo());
        project.setAgileBoardId(entity.getAgileBoardId());
    }

    private void copyPropertiesFromEntityToAo(FoundationProjectEntity project, AOFoundationProjectEntity entity)
    {
        // Don't overwrite values that are null, to prevent clearing values we didn't want to update
        if (project.getKey() != null)
        {
            entity.setKey(project.getKey());
        }

        if (project.getCreatorUserKey() != null)
        {
            entity.setCreatorUserKey(project.getCreatorUserKey());
        }

        if (project.getStatusKey() != null)
        {
            entity.setStatusKey(project.getStatusKey());
        }

        if (project.getLocation() != null)
        {
            entity.setLocation(project.getLocation());
        }

        if (project.getRepo() != null)
        {
            entity.setRepo(project.getRepo());
        }

        if (project.getAgileBoardId() != null)
        {
            entity.setAgileBoardId(project.getAgileBoardId());
        }
    }
}
