package com.atlassian.makeadiff.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * * An Active Objects entity representing a {@link com.atlassian.makeadiff.api.FoundationProjectEntity}.
 */
@Preload
@Table("userprofile")
public interface AOUserProfileEntity extends Entity
{
    String getKey();

    void setKey(String key);

    String getSkills();

    void setSkills(String skills);

    String getDescription();

    void setDescription(String description);
}
