package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.makeadiff.api.UserProfileStore;

@Transactional
public interface AOUserProfileStore extends UserProfileStore
{
}
