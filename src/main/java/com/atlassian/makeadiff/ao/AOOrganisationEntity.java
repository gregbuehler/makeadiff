package com.atlassian.makeadiff.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * An Active Objects entity representing an {@link com.atlassian.makeadiff.api.Organisation}.
 */
@Preload
@Table("organisation")
public interface AOOrganisationEntity extends Entity
{
    String getKey();

    void setKey(String key);

    String getName();

    void setName(String name);

    String getUrl();

    void setUrl(String url);

    String getDescription();

    void setDescription(String description);

    boolean getVerified();

    void setVerified(boolean b);
}
