package com.atlassian.makeadiff.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Table;

/**
 * * An Active Objects entity representing a {@link com.atlassian.makeadiff.api.FoundationProjectEntity}.
 * <p/>
 * This interface will force the table to be automatically created through ActiveObjects
 */
@Preload
@Table("foundationproject")
public interface AOFoundationProjectEntity extends Entity
{
    String getKey();

    void setKey(String key);

    String getCreatorUserKey();

    void setCreatorUserKey(String creatorUserKey);

    String getStatusKey();

    void setStatusKey(String statusKey);

    String getLocation();

    void setLocation(String location);

    String getRepo();

    void setRepo(String repo);

    Long getAgileBoardId();

    void setAgileBoardId(Long agileBoardId);
}
