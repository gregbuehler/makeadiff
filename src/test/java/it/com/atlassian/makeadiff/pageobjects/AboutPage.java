package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page Object for {@link com.atlassian.makeadiff.action.About}
 */
public class AboutPage extends AbstractJiraPage
{
    private final static String URI = "/view/about";
    @ElementBy(id = "about-page")
    private PageElement aboutPage;

    @Override
    public TimedCondition isAt() {
        return aboutPage.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return URI;
    }

}
