package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class ProjectContributorsPage extends ProjectPage
{
    @ElementBy(cssSelector = "table.makeadiff-contributors-table")
    private PageElement contributorsContent;

    public ProjectContributorsPage(String projectKey)
    {
        super(projectKey);
    }

    @Override
    public TimedCondition isAt()
    {
        return Conditions.and(contributorsContent.timed().isPresent(), contributorsNavItem.timed().hasClass("aui-nav-selected"));
    }

    @Override
    public String getUrl()
    {
        return "/browse/" + projectKey + "/contributors";
    }
}
