package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page Object for {@link com.atlassian.makeadiff.action.FoundationHome}
 */
public class HomePage extends AbstractJiraPage
{
    private final static String URI = "/view/home";

    @ElementBy(id = "home-page")
    private PageElement homePage;

    @Override
    public TimedCondition isAt()
    {
        return homePage.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }

}
