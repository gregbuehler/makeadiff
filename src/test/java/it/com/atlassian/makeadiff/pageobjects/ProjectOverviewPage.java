package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page Object for {@link com.atlassian.makeadiff.action.BrowseFoundationProject}
 */
public class ProjectOverviewPage extends ProjectPage
{
    private final static String URI = "/browse/";

    @ElementBy(id = "project-page")
    private PageElement projectPage;

    @ElementBy(id = "volunteer-champion")
    private PageElement volunteerChampionButton;

    public ProjectOverviewPage(String projectKey)
    {
        super(projectKey);
    }

    @Override
    public TimedCondition isAt()
    {
        return Conditions.and(projectPage.timed().isPresent(), overviewNavItem.timed().hasClass("aui-nav-selected"));
    }

    @Override
    public String getUrl()
    {
        return URI + projectKey;
    }

    public TimedCondition hasVolunteerChampionButton()
    {
        return volunteerChampionButton.timed().isPresent();
    }

    public ProjectOverviewPage clickVolunteerChampionButton()
    {
        volunteerChampionButton.click();
        return pageBinder.bind(ProjectOverviewPage.class, projectKey);
    }
}
