package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class SignupPage extends AbstractJiraPage
{
    private final static String URI = "/view/signup";

    @ElementBy(id = "email")
    private PageElement emailInput;

    @ElementBy(id = "fullname")
    private PageElement fullnameInput;

    @ElementBy(id = "password")
    private PageElement passwordInput;

    @ElementBy(cssSelector = "#foundation-sign-up input[type=submit]")
    private PageElement signupButton;

    public String getUrl()
    {
        return URI;
    }

    @Override
    public TimedCondition isAt()
    {
        return emailInput.timed().isPresent();
    }

    public void signup(String email, String fullname, String password)
    {
        emailInput.type(email);
        fullnameInput.type(fullname);
        passwordInput.type(password);
        signupButton.click();

        //return pageBinder.bind(HomePage.class);
    }
}
