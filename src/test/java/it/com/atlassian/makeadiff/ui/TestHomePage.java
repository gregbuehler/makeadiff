package it.com.atlassian.makeadiff.ui;

import it.com.atlassian.makeadiff.pageobjects.HomePage;
import it.com.atlassian.makeadiff.pageobjects.LoginPage;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * WebDriver Test for {@link com.atlassian.makeadiff.action.FoundationHome}
 */
public class TestHomePage extends BaseTest
{
    @Test
    public void testHomePageView()
    {
        jira.goTo(LoginPage.class).loginAsSysAdmin(HomePage.class);
        assertTrue(jira.isAt(HomePage.class));
    }

    @Test
    public void testHomePageViewAnonymous()
    {
        jira.goTo(HomePage.class);
        assertTrue(jira.isAt(HomePage.class));
    }

}
