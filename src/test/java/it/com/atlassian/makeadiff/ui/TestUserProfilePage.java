package it.com.atlassian.makeadiff.ui;

import it.com.atlassian.makeadiff.pageobjects.AboutPage;
import it.com.atlassian.makeadiff.pageobjects.HomePage;
import it.com.atlassian.makeadiff.pageobjects.LoginPage;
import it.com.atlassian.makeadiff.pageobjects.UserProfilePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * WebDriver Test for {@link com.atlassian.makeadiff.action.UserProfileAction}
 */
public class TestUserProfilePage extends BaseTest
{
    @Test
    public void testUserProfilePageViewAnonymousRedirectsToLogin()
    {
        jira.visitDelayed(UserProfilePage.class);
        jira.getPageBinder().bind(LoginPage.class);
        assertTrue(jira.isAt(LoginPage.class));
    }

    @Test
    public void testUserProfilePageView()
    {
        jira.goTo(LoginPage.class).loginAsSysAdmin(UserProfilePage.class);
        assertTrue(jira.isAt(UserProfilePage.class));
    }
}
