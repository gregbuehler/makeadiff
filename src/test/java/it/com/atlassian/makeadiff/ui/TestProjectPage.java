package it.com.atlassian.makeadiff.ui;

import it.com.atlassian.makeadiff.pageobjects.ProjectContributorsPage;
import it.com.atlassian.makeadiff.pageobjects.ProjectOverviewPage;
import it.com.atlassian.makeadiff.pageobjects.ProjectTasksPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * WebDriver Test for {@link com.atlassian.makeadiff.action.BrowseFoundationProject}
 */
public class TestProjectPage extends BaseTest
{
    @Test
    public void testProjectPageViewAnonymous()
    {
        jira.goTo(ProjectOverviewPage.class, MHP_PROJECT_KEY);
    }

    @Test
    public void testHorizontalNav()
    {
        ProjectOverviewPage overviewPage = jira.goTo(ProjectOverviewPage.class, MHP_PROJECT_KEY);
        ProjectTasksPage tasksPage = overviewPage.goToTasks();
        ProjectContributorsPage contributorsPage = tasksPage.goToContributors();
        contributorsPage.goToOverview();
    }
}
