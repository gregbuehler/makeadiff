package it.com.atlassian.makeadiff.ui;

import it.com.atlassian.makeadiff.pageobjects.AboutPage;
import it.com.atlassian.makeadiff.pageobjects.LoginPage;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * WebDriver Test for {@link com.atlassian.makeadiff.action.About}
 */
public class TestAboutPage extends BaseTest
{
    @Test
    public void testLoadAboutPageViewAnonymous()
    {
        jira.goTo(AboutPage.class);
        assertTrue(jira.isAt(AboutPage.class));
    }

    @Test
    public void testLoadAboutPageView()
    {
        jira.goTo(LoginPage.class).loginAsSysAdmin(AboutPage.class);
        assertTrue(jira.isAt(AboutPage.class));
    }

}
