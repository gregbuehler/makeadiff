package com.atlassian.makeadiff.api;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GenerateProjectKeyServiceTest
{
    @Test
    public void testShortSingleWordName() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        assertEquals("BAM", generateProjectKeyService.generateKey("Bamboo"));
        assertEquals("AB", generateProjectKeyService.generateKey("ab"));
        assertEquals("AB", generateProjectKeyService.generateKey("A''''b"));
    }

    @Test
    public void testNameWithNumbers() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        assertEquals("PLI1", generateProjectKeyService.generateKey("Party like it's 1999"));
        assertEquals("1TSTE", generateProjectKeyService.generateKey("1984 is the start of the end"));
    }

    @Test
    public void testLongNameGetsInitials() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        assertEquals("BTA", generateProjectKeyService.generateKey("Bamboo is the awesome"));
        assertEquals("CFTWGJ", generateProjectKeyService.generateKey("Charity For The Win and great justice"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testShittyNamesWithNumber() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        generateProjectKeyService.generateKey("111");
    }

    @Test (expected = IllegalArgumentException.class)
    public void testShittyNamesWithShortText() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        generateProjectKeyService.generateKey("A");
    }
    @Test (expected = IllegalArgumentException.class)
    public void testShittyNamesWithNumbers() throws Exception
    {
        GenerateProjectKeyService generateProjectKeyService = new GenerateProjectKeyService();
        generateProjectKeyService.generateKey("12 12 323 ' 122");
    }


}
