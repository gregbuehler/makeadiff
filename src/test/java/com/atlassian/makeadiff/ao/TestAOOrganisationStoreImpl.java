package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.makeadiff.api.Organisation;
import net.java.ao.EntityManager;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(ActiveObjectsJUnitRunner.class)
public class TestAOOrganisationStoreImpl
{
    private EntityManager entityManager;
    private ActiveObjects activeObjects;
    private AOOrganisationStoreImpl store;

    @Mock private UserUtil userUtil;
    @Mock private GroupManager groupManager;

    @Before
    public void setUp()
    {
        assertNotNull(entityManager);
        activeObjects = new TestActiveObjects(entityManager);
        store = new AOOrganisationStoreImpl(activeObjects, userUtil, groupManager);

        // Prepare AO and make sure it is clear
        activeObjects.migrate(AOOrganisationEntity.class);
        assertEquals(0, activeObjects.find(AOOrganisationEntity.class).length);
    }

    @Test
    public void testAdd()
    {
        store.createOrUpdateOrganisation("The Human Fund", "http://humanfund.org", "Loves humans.", false);
        activeObjects.flushAll();

        final AOOrganisationEntity[] entities = activeObjects.find(AOOrganisationEntity.class);
        assertEquals(1, entities.length);
        assertEquals("The Human Fund", entities[0].getName());
        assertEquals("http://humanfund.org", entities[0].getUrl());
        assertEquals("Loves humans.", entities[0].getDescription());
    }

    @Test
    public void testGetAll()
    {
        createTestOrganisations();

        final Collection<Organisation> found = store.getAllOrganisations();
        assertEquals(3, found.size());
        assertContains(found, "The Human Fund", "http://humanfund.org");
        assertContains(found, "A Worthy Cause", "http://worthycause.org.au");
        assertContains(found, "A Really Great Charity", "www.great.com");

        // Make sure all of the keys are not null and not equal
        final Set<String> keys = new HashSet<String>(3);
        for (final Organisation org : found)
        {
            assertNotNull(org.getKey());
            keys.add(org.getKey());
        }
        assertEquals(3, keys.size());
    }

    @Test
    public void testGetAllWithLimit() throws Exception
    {
        createTestOrganisations();

        final Collection<Organisation> found = store.getAllOrganisations(2);
        assertEquals(2, found.size());
    }

    private void createTestOrganisations()
    {
        store.createOrUpdateOrganisation("The Human Fund", "http://humanfund.org", "Loves humans.", false);
        store.createOrUpdateOrganisation("A Worthy Cause", "http://worthycause.org.au", "Worth it.", false);
        store.createOrUpdateOrganisation("A Really Great Charity", "www.great.com", "Great.", false);
        activeObjects.flushAll();
    }

    @Test
    public void testGetLikeName()
    {
        store.createOrUpdateOrganisation("Test Charity", "", "", false);
        store.createOrUpdateOrganisation("Charity", "", "", false);
        store.createOrUpdateOrganisation("Charity Test", "", "", false);
        activeObjects.flushAll();

        Collection<Organisation> found = store.findOrganisationByLikeName("test");
        assertEquals(2, found.size());
        assertContains(found, "Test Charity", "");
        assertContains(found, "Charity Test", "");

        found = store.findOrganisationByLikeName("Charity");
        assertEquals(3, found.size());

        found = store.findOrganisationByLikeName("None");
        assertEquals(0, found.size());
    }

    private static void assertContains(final Collection<Organisation> orgs, final String name, final String url)
    {
        boolean found = false;
        for (final Organisation org : orgs)
        {
            if (org.getName().equals(name) && org.getUrl().equals(url))
            {
                found = true;
                break;
            }
        }

        assertTrue(found);
    }
}
